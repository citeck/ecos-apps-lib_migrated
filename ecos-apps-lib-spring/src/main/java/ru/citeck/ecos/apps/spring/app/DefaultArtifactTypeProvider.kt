package ru.citeck.ecos.apps.spring.app

import mu.KotlinLogging
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Component
import ru.citeck.ecos.apps.EcosAppsConstants
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.apps.app.domain.artifact.type.ArtifactTypeProvider
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.io.file.mem.EcosMemDir
import ru.citeck.ecos.commons.io.file.std.EcosStdFile
import java.time.Instant
import javax.annotation.PostConstruct

@Component
class DefaultArtifactTypeProvider(
    private val applicationContext: ApplicationContext,
    private val services: EcosAppsServiceFactory
) : ArtifactTypeProvider {

    companion object {

        val log = KotlinLogging.logger {}

        const val TYPES_DIR = EcosAppsConstants.DIR_EAPPS + "/" + EcosAppsConstants.DIR_TYPES
    }

    private val startupTime = Instant.now()

    init {
        services.localAppService.typeProvider = this
    }

    @PostConstruct
    fun init() {
        services.artifactService.loadTypes(getArtifactTypesDir())
    }

    override fun getArtifactTypesDir(): EcosFile {

        val typeDirs = applicationContext.getResources("classpath*:$TYPES_DIR")
            .filter { it.isFile && it.exists() }

        if (typeDirs.isEmpty()) {
            return EcosMemDir()
        }

        if (typeDirs.size == 1) {
            return EcosStdFile(typeDirs[0].file)
        }

        val result = EcosMemDir()
        typeDirs.forEach {
            try {
                result.copyFilesFrom(EcosStdFile(it.file))
            } catch (e: Exception) {
                log.error(e) { "Types reading error. Resource: $it" }
            }
        }

        return result
    }

    override fun getLastModified(): Instant? {
        return startupTime
    }
}
