package ru.citeck.ecos.apps.spring.app

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.citeck.ecos.apps.app.domain.ecostype.service.ModelTypeArtifactResolver
import ru.citeck.ecos.apps.app.domain.ecostype.service.ModelTypeArtifactService
import javax.annotation.PostConstruct

@Component
class ModelTypeArtifactResolverRegistrar(
    private val modelTypeArtifactService: ModelTypeArtifactService
) {

    @Autowired(required = false)
    var modelTypeArtifactResolver: ModelTypeArtifactResolver? = null

    @PostConstruct
    fun init() {
        modelTypeArtifactResolver.let {
            modelTypeArtifactService.modelTypeArtifactResolver = modelTypeArtifactResolver
        }
    }
}
