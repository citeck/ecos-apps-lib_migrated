package ru.citeck.ecos.apps.spring

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = ["ru.citeck.ecos.apps.spring"])
open class EcosAppsAutoConfiguration
