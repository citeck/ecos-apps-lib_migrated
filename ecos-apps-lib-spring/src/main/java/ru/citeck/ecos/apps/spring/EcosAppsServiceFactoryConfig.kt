package ru.citeck.ecos.apps.spring

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceProvider
import ru.citeck.ecos.apps.app.domain.buildinfo.service.BuildInfoProvider
import ru.citeck.ecos.apps.app.domain.buildinfo.service.BuildInfoService
import ru.citeck.ecos.apps.app.domain.ecostype.service.ModelTypeArtifactService
import ru.citeck.ecos.apps.app.domain.handler.ArtifactHandlerService
import ru.citeck.ecos.apps.app.service.LocalAppService
import ru.citeck.ecos.apps.app.service.remote.RemoteAppService
import ru.citeck.ecos.apps.artifact.ArtifactService
import ru.citeck.ecos.apps.artifact.controller.ArtifactControllerService
import ru.citeck.ecos.apps.artifact.type.ArtifactTypeService
import ru.citeck.ecos.apps.eapps.service.LocalEappsService
import ru.citeck.ecos.apps.eapps.service.RemoteEappsService
import ru.citeck.ecos.apps.spring.app.DefaultBuildInfoProvider
import ru.citeck.ecos.commands.CommandsServiceFactory
import ru.citeck.ecos.records3.RecordsServiceFactory
import javax.annotation.PostConstruct

@Configuration
open class EcosAppsServiceFactoryConfig(
    commandsServices: CommandsServiceFactory,
    recordsServices: RecordsServiceFactory,
    private val providerComponents: List<ArtifactSourceProvider>
) : EcosAppsServiceFactory() {

    init {
        this.commandsServices = commandsServices
        this.recordsServices = recordsServices
    }

    @PostConstruct
    override fun init() {
        super.init()
    }

    @Bean
    override fun createArtifactTypeService(): ArtifactTypeService {
        return super.createArtifactTypeService()
    }

    @Bean
    override fun createLocalEappsService(): LocalEappsService {
        return super.createLocalEappsService()
    }

    @Bean
    override fun createArtifactHandlerService(): ArtifactHandlerService {
        return super.createArtifactHandlerService()
    }

    @Bean
    override fun createModelTypeArtifactsService(): ModelTypeArtifactService {
        return super.createModelTypeArtifactsService()
    }

    @Bean
    override fun createRemoteAppService(): RemoteAppService {
        return super.createRemoteAppService()
    }

    @Bean
    override fun createRemoteEappsService(): RemoteEappsService {
        return super.createRemoteEappsService()
    }

    @Bean
    override fun createLocalAppService(): LocalAppService {
        return super.createLocalAppService()
    }

    @Bean
    override fun createArtifactService(): ArtifactService {
        return super.createArtifactService()
    }

    @Bean
    override fun createArtifactControllerService(): ArtifactControllerService {
        return super.createArtifactControllerService()
    }

    @Bean
    override fun createBuildInfoService(): BuildInfoService {
        return super.createBuildInfoService()
    }

    @Bean
    @ConditionalOnMissingBean(BuildInfoProvider::class)
    open fun defaultBuildInfoProvider(): BuildInfoProvider {
        return DefaultBuildInfoProvider()
    }

    override fun createArtifactSourceProviders(): List<ArtifactSourceProvider> {
        return providerComponents
    }
}
