package ru.citeck.ecos.apps.spring.app

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.citeck.ecos.apps.app.domain.buildinfo.service.BuildInfoProvider
import ru.citeck.ecos.apps.app.domain.buildinfo.service.BuildInfoService
import javax.annotation.PostConstruct

@Component
class BuildInfoProviderRegistrar(
    private val buildInfoService: BuildInfoService
) {

    private var providers: List<BuildInfoProvider>? = null

    @PostConstruct
    fun registerAll() {
        providers?.forEach { register(it) }
    }

    private fun register(handler: BuildInfoProvider) {
        buildInfoService.addProvider(handler)
    }

    @Autowired(required = false)
    fun setProviders(providers: List<BuildInfoProvider>?) {
        this.providers = providers
    }
}
