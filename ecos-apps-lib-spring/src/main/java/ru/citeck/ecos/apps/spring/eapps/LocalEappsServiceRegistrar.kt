package ru.citeck.ecos.apps.spring.eapps

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.citeck.ecos.apps.eapps.service.ArtifactUpdater
import ru.citeck.ecos.apps.eapps.service.ArtifactUploader
import ru.citeck.ecos.apps.eapps.service.LocalEappsService
import javax.annotation.PostConstruct

@Component
class LocalEappsServiceRegistrar {

    @Autowired(required = false)
    private var artifactUploader: ArtifactUploader? = null
    @Autowired(required = false)
    private var artifactUpdater: ArtifactUpdater? = null
    @Autowired
    private lateinit var localEappsService: LocalEappsService

    @PostConstruct
    fun init() {
        localEappsService.artifactUploader = artifactUploader
        localEappsService.artifactUpdater = artifactUpdater
    }
}
