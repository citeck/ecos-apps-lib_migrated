package ru.citeck.ecos.apps.spring.app

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.citeck.ecos.apps.app.domain.handler.ArtifactHandlerService
import ru.citeck.ecos.apps.app.domain.handler.EcosArtifactHandler
import javax.annotation.PostConstruct

@Component
class EcosArtifactHandlerRegistrar(
    private val handlerService: ArtifactHandlerService
) {

    companion object {
        val log = KotlinLogging.logger {}
    }

    private var handlers: List<EcosArtifactHandler<*>>? = null

    @PostConstruct
    fun registerAll() {
        handlers?.forEach { register(it) }
    }

    private fun register(handler: EcosArtifactHandler<*>) {

        log.info(
            "Found and registered '" + handler.getArtifactType() + "' handler " +
                "with name: " + handler.javaClass.simpleName
        )

        handlerService.register(handler)
    }

    @Autowired(required = false)
    fun setHandlers(handlers: List<EcosArtifactHandler<*>>?) {
        this.handlers = handlers
    }
}
