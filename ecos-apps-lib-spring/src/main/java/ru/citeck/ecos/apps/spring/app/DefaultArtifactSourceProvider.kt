package ru.citeck.ecos.apps.spring.app

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationContext
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Component
import ru.citeck.ecos.apps.EcosAppsConstants
import ru.citeck.ecos.apps.app.domain.artifact.reader.ArtifactsReader
import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceInfo
import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceProvider
import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceType
import ru.citeck.ecos.apps.app.domain.artifact.source.SourceKey
import ru.citeck.ecos.apps.app.util.AppDirWatchUtils
import ru.citeck.ecos.apps.artifact.type.TypeContext
import ru.citeck.ecos.commons.io.file.std.EcosStdFile
import java.io.File
import java.nio.file.*
import java.time.Instant
import javax.annotation.PostConstruct

@Component
class DefaultArtifactSourceProvider(
    private val applicationContext: ApplicationContext
) : ArtifactSourceProvider {

    companion object {
        val log = KotlinLogging.logger {}

        const val ARTIFACTS_DIR = EcosAppsConstants.DIR_EAPPS + "/" + EcosAppsConstants.DIR_ARTIFACTS

        private val CLASSPATH_SOURCE_KEY = SourceKey("classpath", ArtifactSourceType.APPLICATION)
    }

    @Value("\${eapps.watcher.artifacts.enabled:false}")
    private var watcherEnabled: Boolean = false
    @Value("#{'\${eapps.watcher.artifacts.locations:\"\"}'.split(',')}")
    private var watchLocations: List<String> = emptyList()
    private var watchLocationsPath: List<Path> = emptyList()

    private var artifactsLastModified = Instant.now()

    private lateinit var reader: ArtifactsReader
    private var listener: ((ArtifactSourceInfo) -> Unit)? = null

    @PostConstruct
    fun init() {
        initWatcher()
    }

    override fun init(reader: ArtifactsReader) {
        this.reader = reader
    }

    override fun listenChanges(listener: (ArtifactSourceInfo) -> Unit) {
        this.listener = listener
    }

    private fun initWatcher() {

        if (!watcherEnabled) {
            return
        }
        watchLocationsPath = watchLocations
            .filter { it.isNotBlank() }
            .map { Paths.get(it) }.filter {

                if (!it.toFile().exists()) {
                    log.info { "Location to watch '${it.toAbsolutePath()}' is not exists" }
                    false
                } else {
                    log.info { "Artifacts watch stated for location '${it.toAbsolutePath()}'" }
                    true
                }
            }

        if (watchLocationsPath.isEmpty()) {
            log.warn { "Artifacts watcher enabled, but locations is empty" }
            watcherEnabled = false
            return
        }
        AppDirWatchUtils.watch(
            "eapps artifacts",
            watchLocationsPath,
            listOf(StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY)
        ) { path, event ->
            if (event.kind() == StandardWatchEventKinds.ENTRY_CREATE) {
                log.info { "New file detected: $path" }
            } else {
                log.info { "File change detected: $path" }
            }
            artifactsLastModified = Instant.now()
            listener?.invoke(
                ArtifactSourceInfo.create {
                    withKey(CLASSPATH_SOURCE_KEY)
                    withLastModified(artifactsLastModified)
                }
            )
        }
    }

    override fun getArtifactSources(): List<ArtifactSourceInfo> {

        val result = ArrayList<ArtifactSourceInfo>()

        result.addAll(getBaseSources())

        return result
    }

    private fun getBaseSources(): List<ArtifactSourceInfo> {

        val appsDir = ClassPathResource(ARTIFACTS_DIR)

        if (!appsDir.exists()) {
            return emptyList()
        }

        return listOf(
            ArtifactSourceInfo.create {
                withKey(CLASSPATH_SOURCE_KEY)
                withLastModified(artifactsLastModified)
            }
        )
    }

    override fun getArtifacts(
        source: SourceKey,
        types: List<TypeContext>,
        since: Instant
    ): Map<String, List<Any>> {

        if (source != CLASSPATH_SOURCE_KEY) {
            return emptyMap()
        }

        if (watchLocationsPath.isNotEmpty()) {

            val result = HashMap<String, MutableList<Any>>()
            watchLocationsPath.forEach {
                val artifacts = loadArtifacts(it.toFile(), types)
                artifacts.forEach { (typeId, typeArtifacts) ->
                    result.computeIfAbsent(typeId) { ArrayList() }.addAll(typeArtifacts)
                }
            }
            return result
        }

        return getArtifactsFromClasspath(types)
    }

    private fun getArtifactsFromClasspath(types: List<TypeContext>): Map<String, List<Any>> {

        val artifactDirs = applicationContext.getResources("classpath*:$ARTIFACTS_DIR")
        val result = mutableMapOf<String, MutableList<Any>>()

        artifactDirs
            .filter { it.exists() && it.isFile }
            .forEach { dir ->
                loadArtifacts(dir.file, types).forEach { (typeId, typeArtifacts) ->
                    result.computeIfAbsent(typeId) { ArrayList() }.addAll(typeArtifacts)
                }
            }

        return result
    }

    private fun loadArtifacts(root: File, types: List<TypeContext>): Map<String, MutableList<Any>> {
        val result = mutableMapOf<String, MutableList<Any>>()
        try {
            val artifactsRoot = EcosStdFile(root)
            reader.readArtifacts(artifactsRoot, types).forEach {
                (type, artifacts) ->
                result.computeIfAbsent(type) { ArrayList() }.addAll(artifacts)
            }
        } catch (e: Exception) {
            log.error(e) { "Artifacts reading failed from resource ${root.absolutePath}" }
        }
        return result
    }

    override fun isStatic(): Boolean {
        return true
    }
}
