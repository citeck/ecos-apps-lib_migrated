package ru.citeck.ecos.apps.spring.app

import mu.KotlinLogging
import org.springframework.util.ResourceUtils
import ru.citeck.ecos.apps.app.domain.buildinfo.dto.BuildInfo
import ru.citeck.ecos.apps.app.domain.buildinfo.service.BuildInfoProvider
import ru.citeck.ecos.commons.json.Json.mapper
import java.io.FileNotFoundException

class DefaultBuildInfoProvider : BuildInfoProvider {

    companion object {
        private val log = KotlinLogging.logger {}
    }

    private val lazyInfo: List<BuildInfo> by lazy { loadBuildInfo() }

    override fun getBuildInfo(): List<BuildInfo> {
        return lazyInfo
    }

    private fun loadBuildInfo(): List<BuildInfo> {

        var buildInfo: BuildInfo? = null

        try {
            val buildInfoFile = ResourceUtils.getFile("classpath:build-info/full.json")
            if (buildInfoFile.exists()) {
                buildInfo = mapper.read(buildInfoFile, BuildInfo::class.java)
            }
        } catch (e: FileNotFoundException) {
            // do nothing
        } catch (e: Exception) {
            log.error("Build info reading failed", e)
        }

        return if (buildInfo != null) {
            listOf(buildInfo)
        } else {
            emptyList()
        }
    }
}
