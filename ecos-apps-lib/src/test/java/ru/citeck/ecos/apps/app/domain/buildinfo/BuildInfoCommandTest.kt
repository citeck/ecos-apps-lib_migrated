package ru.citeck.ecos.apps.app.domain.buildinfo

import com.github.fridujo.rabbitmq.mock.MockConnectionFactory
import org.junit.jupiter.api.Test
import ru.citeck.ecos.apps.app.TestApp
import ru.citeck.ecos.apps.app.domain.buildinfo.dto.BuildInfo
import ru.citeck.ecos.apps.app.domain.buildinfo.dto.CommitInfo
import ru.citeck.ecos.apps.app.domain.buildinfo.dto.CommitPersonInfo
import ru.citeck.ecos.apps.app.domain.buildinfo.service.BuildInfoProvider
import ru.citeck.ecos.commons.data.MLText
import ru.citeck.ecos.rabbitmq.RabbitMqConn
import java.time.Instant
import java.util.concurrent.TimeUnit
import kotlin.test.assertEquals

class BuildInfoCommandTest {

    private lateinit var buildInfo: List<BuildInfo>

    @Test
    fun test() {

        buildInfo = listOf(
            BuildInfo.create {
                this.withRepo("http://some-url.git")
                this.withBranch("develop")
                this.withName(MLText("Test info"))
                this.withVersion("1.0.0-SNAPSHOT")
                this.withBuildDate(Instant.parse("2021-01-01T11:11:11Z"))
                this.withCommits(
                    listOf(
                        CommitInfo.create {
                            this.withBody("commit-body")
                            this.withSubject("subject")
                            this.withAuthor(
                                CommitPersonInfo.create {
                                    this.withName("Pavel")
                                    this.withEmail("pavel@email.com")
                                }
                            )
                        },
                        CommitInfo.create {
                            this.withBody("commit-body")
                            this.withSubject("subject")
                            this.withAuthor(
                                CommitPersonInfo.create {
                                    this.withName("Pavel")
                                    this.withEmail("pavel@email.com")
                                }
                            )
                        }
                    )
                )
            }
        )

        val rabbitConnectionFactory = MockConnectionFactory()
        rabbitConnectionFactory.host = "localhost"
        rabbitConnectionFactory.username = "admin"
        rabbitConnectionFactory.password = "admin"

        val rabbitMqConn = RabbitMqConn(rabbitConnectionFactory)
        rabbitMqConn.waitUntilReady(2_000)

        val testApp = TestApp("test-app", rabbitMqConn)

        testApp.services.buildInfoService.addProvider(TestBuildInfoProvider())

        val buildInfoResult = testApp.services.remoteAppService.getBuildInfo("test-app").get(5, TimeUnit.SECONDS)
        assertEquals(ArrayList(buildInfo), ArrayList(buildInfoResult))

        val buildInfoResult2 = testApp.services.remoteAppService.getBuildInfo(
            "test-app",
            buildInfo[0].buildDate
        ).get(5, TimeUnit.SECONDS)
        assertEquals(0, buildInfoResult2.size)

        val buildInfoResult3 = testApp.services.remoteAppService.getBuildInfo(
            "test-app",
            buildInfo[0].buildDate.minusMillis(1)
        ).get(5, TimeUnit.SECONDS)
        assertEquals(ArrayList(buildInfo), ArrayList(buildInfoResult3))

        val buildInfoResultForAll = testApp.services.remoteAppService.getBuildInfo().get(5, TimeUnit.SECONDS)
        assertEquals(ArrayList(buildInfo), ArrayList(buildInfoResultForAll))
    }

    inner class TestBuildInfoProvider : BuildInfoProvider {
        override fun getBuildInfo(): List<BuildInfo> {
            return buildInfo
        }
    }
}
