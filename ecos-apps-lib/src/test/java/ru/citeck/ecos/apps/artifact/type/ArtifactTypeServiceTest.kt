package ru.citeck.ecos.apps.artifact.type

import org.junit.jupiter.api.Test
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.io.file.mem.EcosMemDir
import kotlin.test.assertEquals

class ArtifactTypeServiceTest {

    @Test
    fun test() {

        val typeService = ArtifactTypeService()

        val root = EcosMemDir()
        root.createFile(
            "test-type/test/type.yml",
            """
            modelVersion: "1.0"
            source-id: "notification"
            controller:
                type: json
                config:
                    a: b
                    c: d
            """.trimIndent()
        )

        root.createFile(
            "test-type/test2/type.yml",
            """
            modelVersion: "1.0"
            source-id: "notification"
            controller:
                type: json
                config:
                    e: f
                    g: h
            """.trimIndent()
        )

        val types = typeService.loadTypes(root)
        validateTypes(typeService)

        val newRoot = EcosMemDir()
        types.forEach {
            newRoot.createDir(it.getId()).copyFilesFrom(it.getContent())
        }

        val typeService2 = ArtifactTypeService()
        typeService2.loadTypes(newRoot)
        validateTypes(typeService2)

        val typeService3 = ArtifactTypeService()
        types.forEach {
            typeService3.loadType(it.getId(), it.getContent())
        }
        validateTypes(typeService3)
    }

    private fun validateTypes(typeService: ArtifactTypeService) {

        assertEquals(2, typeService.getAllTypes().size)

        val testType = typeService.getType("test-type/test")!!
        val test2Type = typeService.getType("test-type/test2")!!

        assertEquals("test-type/test", testType.getId())
        assertEquals("test-type/test2", test2Type.getId())

        assertEquals(ObjectData.create("{\"a\":\"b\",\"c\":\"d\"}"), testType.getMeta().controller.config)
        assertEquals(ObjectData.create("{\"e\":\"f\",\"g\":\"h\"}"), test2Type.getMeta().controller.config)
    }
}
