package ru.citeck.ecos.apps.artifact.controller.type

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import ru.citeck.ecos.apps.artifact.controller.type.binary.BinArtifactController
import ru.citeck.ecos.commons.io.file.std.EcosStdFile
import java.io.File

class BinArtifactControllerSimpleTest {

    companion object {
        private val ROOT = File("./src/test/resources/test/BinArtifactControllerSimpleTest")
    }

    @Test
    fun simpleTest() {

        val testRoot = File(ROOT, "simple")
        val template = File(testRoot, "template.ftl")

        val controller = BinArtifactController()
        val config = BinArtifactController.Config()

        val artifacts = controller.read(EcosStdFile(testRoot), config)
        assertThat(artifacts).hasSize(1)

        val artifact = artifacts[0]

        assertThat(artifact.data).isEqualTo(EcosStdFile(template).readAsBytes())

        BinArtifactsTestUtils.checkArtifact(
            controller,
            config,
            artifact,
            emptyMap(),
            "template.ftl"
        )
    }

    @Test
    fun innerTest() {

        val testRoot = File(ROOT, "simpleInner")
        val template = File(testRoot, "inner/template.ftl")

        val controller = BinArtifactController()
        val config = BinArtifactController.Config()

        val artifacts = controller.read(EcosStdFile(testRoot), config)
        assertThat(artifacts).hasSize(1)

        val artifact = artifacts[0]

        assertThat(artifact.data).isEqualTo(EcosStdFile(template).readAsBytes())

        BinArtifactsTestUtils.checkArtifact(
            controller,
            config,
            artifact,
            emptyMap(),
            "inner/template.ftl"
        )
    }

    @Test
    fun innerInnerTest() {

        val testRoot = File(ROOT, "simpleInnerInner")
        val template = File(testRoot, "inner/inner/template.ftl")

        val controller = BinArtifactController()
        val config = BinArtifactController.Config()

        val artifacts = controller.read(EcosStdFile(testRoot), config)
        assertThat(artifacts).hasSize(1)

        val artifact = artifacts[0]

        assertThat(artifact.data).isEqualTo(EcosStdFile(template).readAsBytes())

        BinArtifactsTestUtils.checkArtifact(
            controller,
            config,
            artifact,
            emptyMap(),
            "inner/inner/template.ftl"
        )
    }
}
