package ru.citeck.ecos.apps.artifact.controller.type

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Test
import ru.citeck.ecos.apps.artifact.controller.type.binary.BinArtifact
import ru.citeck.ecos.apps.artifact.controller.type.binary.BinArtifactController
import ru.citeck.ecos.commons.data.MLText
import ru.citeck.ecos.commons.io.file.mem.EcosMemDir
import ru.citeck.ecos.commons.io.file.std.EcosStdFile
import ru.citeck.ecos.commons.json.Json
import ru.citeck.ecos.commons.utils.ZipUtils
import java.io.File
import kotlin.test.assertEquals

class BinArtifactControllerGroupTest {

    companion object {
        private val ROOT = File("./src/test/resources/test/BinArtifactControllerGroupTest")
    }

    @Test
    fun testMLArtifact() {

        val metaFile = EcosStdFile(File(ROOT, "mlArtifact/test-file.meta.yml"))

        val ruFileName = "test-file_ru.ftl"
        val enFileName = "test-file_en.ftl"

        val ruFileContent = EcosStdFile(File(ROOT, "mlArtifact/$ruFileName")).readAsBytes()
        val enFileContent = EcosStdFile(File(ROOT, "mlArtifact/$enFileName")).readAsBytes()

        val metaFromFile = Json.mapper.read(metaFile, MetaDto::class.java)

        val mlArtifactRoot = EcosStdFile(File(ROOT, "mlArtifact"))

        val controller = BinArtifactController()
        val config = BinArtifactController.Config(
            artifactNamePattern = "(.+)_\\w+.ftl",
            artifactNameGroupIdx = 1
        )

        val artifact = controller.read(mlArtifactRoot, config)[0]

        assertEquals(metaFromFile, artifact.meta.getAs(MetaDto::class.java))

        val extractedZip = ZipUtils.extractZip(artifact.data)
        assertArrayEquals(extractedZip.getFile(ruFileName)!!.readAsBytes(), ruFileContent)
        assertArrayEquals(extractedZip.getFile(enFileName)!!.readAsBytes(), enFileContent)

        val outputDir = EcosMemDir()
        controller.write(outputDir, artifact, config)

        assertEquals(3, outputDir.getChildren().size)

        val artifact1 = controller.read(outputDir, config)[0]
        assertEquals(metaFromFile, artifact1.meta.getAs(MetaDto::class.java))

        val extractedZip1 = ZipUtils.extractZip(artifact1.data)
        assertArrayEquals(extractedZip1.getFile(ruFileName)!!.readAsBytes(), ruFileContent)
        assertArrayEquals(extractedZip1.getFile(enFileName)!!.readAsBytes(), enFileContent)
    }

    @Test
    fun testSimpleArtifact() {

        val metaFile = EcosStdFile(File(ROOT, "simpleArtifact/test-file.ftl.meta.yml"))

        val artifactFileName = "test-file.ftl"
        val artifactFileContent = EcosStdFile(File(ROOT, "simpleArtifact/$artifactFileName")).readAsBytes()

        val metaFromFile = Json.mapper.read(metaFile, MetaDto::class.java)
        val artifactRoot = EcosStdFile(File(ROOT, "simpleArtifact"))

        val controller = BinArtifactController()
        val config = BinArtifactController.Config(
            artifactNamePattern = "(.+)_\\w+.ftl",
            artifactNameGroupIdx = 1
        )

        val artifact = controller.read(artifactRoot, config)[0]

        assertEquals(metaFromFile, artifact.meta.getAs(MetaDto::class.java))

        val extractedZip = ZipUtils.extractZip(artifact.data)
        assertArrayEquals(extractedZip.getFile(artifactFileName)!!.readAsBytes(), artifactFileContent)

        val outputDir = EcosMemDir()
        controller.write(outputDir, artifact, config)

        assertEquals(2, outputDir.getChildren().size)

        val artifact1 = controller.read(outputDir, config)[0]
        assertEquals(metaFromFile, artifact1.meta.getAs(MetaDto::class.java))

        val extractedZip1 = ZipUtils.extractZip(artifact1.data)
        assertArrayEquals(extractedZip1.getFile(artifactFileName)!!.readAsBytes(), artifactFileContent)
    }

    @Test
    fun testInnerArtifacts() {

        val artifactRoot = EcosStdFile(File(ROOT, "innerArtifacts"))

        val controller = BinArtifactController()
        val config = BinArtifactController.Config(
            artifactNamePattern = "(.+)_\\w+.ftl",
            artifactNameGroupIdx = 1
        )

        val artifacts = controller.read(artifactRoot, config)
        assertThat(artifacts).hasSize(4)

        val checkArtifactMeta = { artifact: BinArtifact, expectedMeta: Map<String, String>, expectedPath: String ->
            BinArtifactsTestUtils.checkArtifact(controller, config, artifact, expectedMeta, expectedPath)
        }

        val langArtifact = artifacts.find { it.path.contains("/inner-lang-template.html") }!!
        checkArtifactMeta(
            langArtifact,
            mapOf(
                "/props/title" to "title from inner lang with file meta",
                "/props/dirMeta" to "value from global meta"
            ),
            "inner-with-file-meta/inner-lang-template.html.zip"
        )

        val simpleArtifact = artifacts.find { it.path.contains("/inner-template.html") }!!
        checkArtifactMeta(
            simpleArtifact,
            mapOf(
                "/props/title" to "title from inner with file meta",
                "/props/testProp" to "prop inner"
            ),
            "inner-with-file-meta/inner-template.html.ftl.zip"
        )

        val langInnerInnerArtifact = artifacts.find { it.path.contains("/inner-inner-lang-template.html") }!!
        checkArtifactMeta(
            langInnerInnerArtifact,
            mapOf(
                "/props/title" to "title from inner inner lang with file meta",
                "/props/dirMeta" to "value from global meta"
            ),
            "inner-with-file-meta/inner-inner/inner-inner-lang-template.html.zip"
        )

        val simpleInnerInnerArtifact = artifacts.find { it.path.contains("/inner-inner-template.html") }!!
        checkArtifactMeta(
            simpleInnerInnerArtifact,
            mapOf(
                "/props/title" to "title from inner inner with file meta",
                "/props/testProp" to "prop inner inner",
                "/props/ext" to "ftl inner inner"
            ),
            "inner-with-file-meta/inner-inner/inner-inner-template.html.ftl.zip"
        )
    }

    data class MetaDto(
        val id: String,
        val name: MLText
    )
}
