package ru.citeck.ecos.apps.app

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.apps.artifact.ArtifactRef
import ru.citeck.ecos.apps.artifact.controller.patch.ArtifactPatch
import ru.citeck.ecos.apps.artifact.controller.patch.ArtifactPatchArtifact
import ru.citeck.ecos.apps.artifact.controller.patch.override.OverrideMeta
import ru.citeck.ecos.apps.artifact.controller.patch.override.OverridePatch
import ru.citeck.ecos.commands.CommandsServiceFactory
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.io.file.std.EcosStdFile
import ru.citeck.ecos.commons.json.Json
import ru.citeck.ecos.records3.RecordsServiceFactory
import java.io.File

class OverrideTest {

    @Test
    fun test() {
        val factory = EcosAppsServiceFactory()
        factory.recordsServices = RecordsServiceFactory()
        factory.commandsServices = CommandsServiceFactory()

        val testRoot = EcosStdFile(File("./src/test/resources/override-test"))
        testDir(factory, testRoot, "without-meta", OverrideMeta(-100f, ""))
        testDir(factory, testRoot, "with-meta", OverrideMeta(-500f, "test-scope"))
    }

    private fun testDir(factory: EcosAppsServiceFactory, testRoot: EcosFile, dir: String, expectedMeta: OverrideMeta) {

        val types = factory.artifactService.loadTypes(testRoot.getDir("types")!!)

        val artifacts = factory.artifactService.readArtifacts(testRoot.getDir(dir)!!, types)

        assertThat(artifacts).hasSize(2)
        val patches = artifacts["app/artifact-patch"]!!
        assertThat(patches).hasSize(1)
        val patch = Json.mapper.convert(patches[0], ArtifactPatchArtifact::class.java)!!

        assertThat(patch.order).isEqualTo(expectedMeta.order)
        val artifactRef = ArtifactRef.create("app/some", "some-artifact-0")
        assertThat(patch.id).isEqualTo(
            if (expectedMeta.scope.isNotEmpty()) {
                "override_" + expectedMeta.scope + "$" + artifactRef
            } else {
                "override$$artifactRef"
            }
        )

        val typeCtx = types.find { it.getId() == "app/some" }!!
        val overridePatch = Json.mapper.convert(patch.config, OverridePatch::class.java)!!

        val overriddenArtifact = ObjectData.create(
            factory.artifactService.readArtifactFromBytes(typeCtx, overridePatch.data)
        )
        assertThat(overriddenArtifact.get("/data/some").asText()).isEqualTo("overriddenData")

        val appSomeArtifacts = artifacts["app/some"]!!
        assertThat(appSomeArtifacts).hasSize(1)

        val patchedArtifact = factory.artifactService.applyPatches(
            typeCtx.getId(),
            appSomeArtifacts[0],
            listOf(
                ArtifactPatch(
                    patch.order,
                    patch.type,
                    patch.config
                )
            )
        )

        val patchedArtifactObj = ObjectData.create(patchedArtifact)
        assertThat(patchedArtifactObj.get("/data/some").asText()).isEqualTo("overriddenData")
    }
}
