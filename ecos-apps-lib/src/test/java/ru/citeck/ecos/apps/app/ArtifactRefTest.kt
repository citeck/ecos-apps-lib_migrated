package ru.citeck.ecos.apps.app

import ecos.com.fasterxml.jackson210.databind.ObjectMapper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import ru.citeck.ecos.apps.artifact.ArtifactRef
import ru.citeck.ecos.apps.artifact.ArtifactRef.Companion.create
import ru.citeck.ecos.apps.artifact.ArtifactRef.Companion.valueOf

internal class ArtifactRefTest {

    @Test
    fun test() {

        val type0 = "type0"
        val id0 = "id0"
        val ref = create(type0, id0)

        Assertions.assertEquals(type0, ref.type)
        Assertions.assertEquals(id0, ref.id)

        val mapper = ObjectMapper()
        val typeWithRef = TypeWithRef(ref)

        val strValue = mapper.writeValueAsString(typeWithRef)
        Assertions.assertEquals("{\"ref\":\"$type0$$id0\"}", strValue)

        var refFromStr = mapper.readValue(strValue, TypeWithRef::class.java)
        Assertions.assertEquals(ref, refFromStr.ref)

        val customRefFromStr = valueOf("$type0$$id0")
        Assertions.assertEquals(ref, customRefFromStr)

        var emptyRef = "{\"ref\":\"\"}"
        refFromStr = mapper.readValue(emptyRef, TypeWithRef::class.java)
        Assertions.assertSame(ArtifactRef.EMPTY, refFromStr.ref)

        emptyRef = "{\"ref\":\"$\"}"
        refFromStr = mapper.readValue(emptyRef, TypeWithRef::class.java)
        Assertions.assertSame(ArtifactRef.EMPTY, refFromStr.ref)

        emptyRef = "{\"ref\":\"    \"}"
        refFromStr = mapper.readValue(emptyRef, TypeWithRef::class.java)
        Assertions.assertSame(ArtifactRef.EMPTY, refFromStr.ref)
    }

    class TypeWithRef(var ref: ArtifactRef? = null)
}
