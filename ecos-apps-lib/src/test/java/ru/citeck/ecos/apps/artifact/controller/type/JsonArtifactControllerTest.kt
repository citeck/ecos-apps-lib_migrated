package ru.citeck.ecos.apps.artifact.controller.type

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.apps.artifact.controller.patch.ArtifactPatch
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.data.MLText
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.io.file.mem.EcosMemDir
import ru.citeck.ecos.commons.io.file.mem.EcosMemFile
import ru.citeck.ecos.commons.json.Json
import ru.citeck.ecos.records2.RecordRef
import ru.citeck.ecos.records3.RecordsServiceFactory
import ru.citeck.ecos.records3.record.request.RequestContext
import kotlin.test.assertEquals

internal class JsonArtifactControllerTest {
    companion object {
        private val TEST_SCHEMA_JSON: String = """
            {
                "properties": {
                    "param1": {
                        "type": "integer"
                    },
                    "param2": {
                        "type": "integer"
                    },
                    "id": {
                        "type": "string"
                    }
                }
            }
        """.trimIndent()
        val schemaAsEcosFile: EcosFile = EcosMemFile(null, "", TEST_SCHEMA_JSON.toByteArray())
    }

    @Test
    fun read_filesWithoutSchema_returnAll() {

        val services = EcosAppsServiceFactory()
        services.recordsServices = RecordsServiceFactory()
        val controller = JsonArtifactController(services)

        val root: EcosFile = EcosMemDir()

        val validFile = JsonClassWithValidSchema(1, 1)
        root.createFile("1.json") { output ->
            Json.mapper.write(output, validFile)
        }
        val invalidFile = JsonClassWithInvalidSchema("param1", "param2")
        root.createFile("2.json") { output ->
            Json.mapper.write(output, invalidFile)
        }

        val config = JsonArtifactController.Config()

        val read = controller.read(root, config)

        assertEquals(ObjectData.create(validFile), read.find { it.get("id").asText() == "Valid" })
        assertEquals(ObjectData.create(invalidFile), read.find { it.get("id").asText() == "Invalid" })
    }

    @Test
    fun read_filesWithSchema_returnValid() {

        val services = EcosAppsServiceFactory()
        services.recordsServices = RecordsServiceFactory()
        val controller = JsonArtifactController(services)

        val root: EcosFile = EcosMemDir()

        val validFile = JsonClassWithValidSchema(1, 2)
        root.createFile("1.json") { output ->
            Json.mapper.write(output, validFile)
        }
        val invalidFile = JsonClassWithInvalidSchema("param1", "param2")
        root.createFile("2.json") { output ->
            Json.mapper.write(output, invalidFile)
        }

        val config = JsonArtifactController.Config(schema = schemaAsEcosFile)

        val read = controller.read(root, config)

        assertEquals(2, read.size)
        assertEquals(ObjectData.create(validFile), read[0])
    }

    @Test
    fun getMetaTest() {

        val services = EcosAppsServiceFactory()
        services.recordsServices = RecordsServiceFactory()
        val controller = JsonArtifactController(services)

        val artifact = ObjectData.create(
            """
            {
                "id": "test",
                "name": {
                    "ru": "Название",
                    "en": "Name"
                },
                "name2": {
                    "ru": "Название2",
                    "en": "Name2"
                },
                "parent": "emodel/type@base",
                "parent2": "",
                "parent3": ["emodel/type@base2", "emodel/type@base3", "emodel/type@base"],
                "tags": ["aa", "bb"]
            }
            """.trimIndent()
        )

        val config = JsonArtifactController.Config().copy(
            metaFields = JsonArtifactController.MetaFields()
                .copy(
                    dependencies = listOf(
                        "parent",
                        "parent2!'emodel/type@default'",
                        "parent3[]"
                    )
                )
        )

        val meta = controller.getMeta(artifact, config)

        assertEquals(artifact.get("id").asText(), meta.id)
        assertEquals(artifact.get("name").getAs(MLText::class.java), meta.name)
        assertEquals(
            arrayListOf(
                "emodel/type@base",
                "emodel/type@default",
                "emodel/type@base2",
                "emodel/type@base3"
            ).map { RecordRef.valueOf(it) },
            ArrayList(meta.dependencies)
        )
        assertEquals(arrayListOf("aa", "bb"), ArrayList(meta.tags))

        val meta2 = controller.getMeta(
            artifact,
            config.copy(
                metaFields = JsonArtifactController.MetaFields()
                    .copy(
                        name = "name2"
                    )
            )
        )
        assertEquals(artifact.get("name2").getAs(MLText::class.java), meta2.name)
    }

    @Test
    fun patch_module() {

        val services = EcosAppsServiceFactory()
        services.recordsServices = RecordsServiceFactory()
        val controller = JsonArtifactController(services)

        val original = ObjectData.create("{\"actions\":[\"first\", \"second\"], \"strField\":\"strValue\"}")
        val module = original.deepCopy()

        val patch0 = ArtifactPatch(
            0f,
            "json",
            ObjectData.create(
                """
                {
                "operations":[
                    {"op": "add", "path": "actions", "value": "third"},
                    {"op": "remove", "path": "$.actions[?(@ == \"first\")]"},
                    {"op": "set", "path": "$.inner0.inner1", "value": "abcd"},
                    {"op": "set", "path": "strField", "value": null},
                    {"op": "add", "path": "actions", "value": ["fourth", "fifth"], "idx": 1 }
                ]
                }
            """
            )
        )

        val newModule = controller.applyPatch(module, JsonArtifactController.Config(), patch0)
        assertEquals(
            ObjectData.create(
                """
            {
                "actions": ["second", "fourth", "fifth", "third"],
                "inner0": { "inner1": "abcd" },
                "strField": null
            }
        """
            ),
            newModule
        )
        assertEquals(original, module)
    }

    @Test
    fun idTemplateTest() {

        val services = EcosAppsServiceFactory()
        services.recordsServices = RecordsServiceFactory()
        val controller = JsonArtifactController(services)

        val artifact0 = ObjectData.create(
            """
            {
                "id": "someId",
                "scope": "scope"
            }
            """.trimIndent()
        )

        val config = JsonArtifactController.Config(idTemplate = "\${scope!\$appName}/\${id}")
        val meta0 = controller.getMeta(artifact0, config)

        assertThat(meta0.id).isEqualTo("scope/someId")

        val artifact1 = ObjectData.create(
            """
            {
                "id": "someId",
                "scope": ""
            }
            """.trimIndent()
        )

        RequestContext.doWithAtts(
            mapOf(
                "appName" to "ecos-apps"
            )
        ) { _ ->
            val meta1 = controller.getMeta(artifact1, config)
            assertThat(meta1.id).isEqualTo("ecos-apps/someId")
        }

        val meta2 = controller.getMeta(artifact1, config)
        assertThat(meta2.id).isEqualTo("/someId")

        val config2 = JsonArtifactController.Config(idTemplate = "\${scope!\$appName{?str|presuf('app/','/')}}\${id}")

        RequestContext.doWithAtts(
            mapOf(
                "appName" to "ecos-apps"
            )
        ) { _ ->
            val meta3 = controller.getMeta(artifact1, config2)
            assertThat(meta3.id).isEqualTo("app/ecos-apps/someId")
        }
        val meta4 = controller.getMeta(artifact1, config2)
        assertThat(meta4.id).isEqualTo("someId")

        val dir = EcosMemDir()
        dir.createFile(
            "artifact-0.json",
            Json.mapper.toString(
                ObjectData.create(
                    """
            {
                "id": "artifact-0"
            }
                    """.trimIndent()
                )
            )!!
        )
        dir.createFile(
            "artifact-1.json",
            Json.mapper.toString(
                ObjectData.create(
                    """
            {
                "id": "artifact-1",
                "att-with-default": "value"
            }
                    """.trimIndent()
                )
            )!!
        )

        val artifactsWithDefault = controller.read(
            dir,
            JsonArtifactController.Config(
                attsValues = mapOf("att-with-default" to DataValue.create("\${att-with-default!'def-value'}"))
            )
        )

        assertThat(
            artifactsWithDefault.first {
                it.get("id").asText() == "artifact-0"
            }.get("att-with-default").asText()
        ).isEqualTo("def-value")

        assertThat(
            artifactsWithDefault.first {
                it.get("id").asText() == "artifact-1"
            }.get("att-with-default").asText()
        ).isEqualTo("value")
    }

    private data class JsonClassWithValidSchema(val param1: Int, val param2: Int, val id: String = "Valid")
    private data class JsonClassWithInvalidSchema(val invalidParam1: String, val param2: String, val id: String = "Invalid")
}
