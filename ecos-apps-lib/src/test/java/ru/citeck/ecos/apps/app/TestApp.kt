package ru.citeck.ecos.apps.app

import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.apps.app.domain.artifact.reader.ArtifactsReader
import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceInfo
import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceProvider
import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceType
import ru.citeck.ecos.apps.app.domain.artifact.source.SourceKey
import ru.citeck.ecos.apps.app.domain.artifact.type.ArtifactTypeProvider
import ru.citeck.ecos.apps.artifact.type.TypeContext
import ru.citeck.ecos.commands.CommandsProperties
import ru.citeck.ecos.commands.CommandsServiceFactory
import ru.citeck.ecos.commands.rabbit.RabbitCommandsService
import ru.citeck.ecos.commands.remote.RemoteCommandsService
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.io.file.std.EcosStdFile
import ru.citeck.ecos.rabbitmq.RabbitMqConn
import ru.citeck.ecos.records3.RecordsServiceFactory
import java.io.File
import java.time.Instant
import java.util.concurrent.atomic.AtomicInteger

class TestApp(val name: String, val rabbitMqConn: RabbitMqConn) {

    companion object {
        val instanceCounters = HashMap<String, AtomicInteger>()

        fun clear() {
            instanceCounters.clear()
        }
    }

    val instanceId = name + "_" + instanceCounters.computeIfAbsent(name) { AtomicInteger() }.getAndIncrement()

    val services: EcosAppsServiceFactory

    val types: List<TypeContext>
    val typesDir: EcosFile

    init {

        val commandsServiceFactory = object : CommandsServiceFactory() {
            override fun createProperties(): CommandsProperties {
                val props = super.createProperties()
                props.appInstanceId = instanceId
                props.appName = name
                return props
            }

            override fun createRemoteCommandsService(): RemoteCommandsService {
                return RabbitCommandsService(this, rabbitMqConn)
            }
        }

        commandsServiceFactory.remoteCommandsService

        services = object : EcosAppsServiceFactory() {
            override fun createArtifactSourceProviders(): List<ArtifactSourceProvider> {
                return listOf(SourceProvider(instanceId))
            }
        }
        services.commandsServices = commandsServiceFactory
        services.recordsServices = RecordsServiceFactory()

        TypeProvider(instanceId, services)

        services.init()

        typesDir = services.localAppService.getArtifactTypesDir()
        types = services.artifactService.loadTypes(typesDir)
    }

    private class TypeProvider(val instanceId: String, services: EcosAppsServiceFactory) : ArtifactTypeProvider {

        init {
            services.localAppService.typeProvider = this
        }

        override fun getArtifactTypesDir(): EcosFile {
            return EcosStdFile(File("./src/test/resources/apps/$instanceId/module-types"))
        }

        override fun getLastModified(): Instant? = null
    }

    private class SourceProvider(val instanceId: String) : ArtifactSourceProvider {

        private lateinit var reader: ArtifactsReader

        override fun init(reader: ArtifactsReader) {
            this.reader = reader
        }

        override fun listenChanges(listener: (ArtifactSourceInfo) -> Unit) {
        }

        override fun isStatic(): Boolean {
            return true
        }

        override fun getArtifactSources(): List<ArtifactSourceInfo> {
            return listOf(
                ArtifactSourceInfo.create {
                    withKey(instanceId, ArtifactSourceType.APPLICATION)
                }
            )
        }

        override fun getArtifacts(
            source: SourceKey,
            types: List<TypeContext>,
            since: Instant
        ): Map<String, List<Any>> {
            val root = EcosStdFile(File("./src/test/resources/apps/$instanceId/ecos-app/module"))
            return reader.readArtifacts(root, types)
        }
    }
}
