package ru.citeck.ecos.apps.artifact.controller.type

import org.assertj.core.api.Assertions
import ru.citeck.ecos.apps.artifact.controller.type.binary.BinArtifact
import ru.citeck.ecos.apps.artifact.controller.type.binary.BinArtifactController
import ru.citeck.ecos.commons.io.file.mem.EcosMemDir

object BinArtifactsTestUtils {

    fun checkArtifact(
        controller: BinArtifactController,
        config: BinArtifactController.Config,
        artifact: BinArtifact,
        expectedMeta: Map<String, String>,
        expectedPath: String
    ) {

        Assertions.assertThat(artifact.path).isEqualTo(expectedPath)

        expectedMeta.forEach { (k, v) ->
            Assertions.assertThat(artifact.meta.get(k).asText()).isEqualTo(v)
        }
        val output0 = EcosMemDir()
        controller.write(output0, artifact, config)

        val artifact1 = controller.read(output0, config)[0]

        Assertions.assertThat(artifact1.path).isEqualTo(expectedPath)

        expectedMeta.forEach { (k, v) ->
            Assertions.assertThat(artifact1.meta.get(k).asText()).isEqualTo(v)
        }
    }
}
