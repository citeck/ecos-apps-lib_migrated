package ru.citeck.ecos.apps.app

import com.github.fridujo.rabbitmq.mock.MockConnectionFactory
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceType
import ru.citeck.ecos.apps.app.domain.artifact.source.SourceKey
import ru.citeck.ecos.apps.app.domain.handler.ArtifactHandlerService
import ru.citeck.ecos.apps.app.domain.handler.EcosArtifactHandler
import ru.citeck.ecos.apps.app.service.remote.RemoteAppService
import ru.citeck.ecos.apps.artifact.ArtifactService
import ru.citeck.ecos.apps.artifact.controller.type.binary.BinArtifact
import ru.citeck.ecos.apps.artifact.controller.type.file.FileArtifact
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.io.file.std.EcosStdFile
import ru.citeck.ecos.commons.utils.ZipUtils
import ru.citeck.ecos.rabbitmq.RabbitMqConn
import java.io.File
import java.time.Instant
import java.util.function.Consumer
import kotlin.streams.toList

const val FIRST_APP_NAME = "first"
const val SECOND_APP_NAME = "second"

class ArtifactsReadWriteTest {

    private lateinit var firstApp: TestApp
    private lateinit var secondApp: TestApp

    private lateinit var artifactService0: ArtifactService
    private lateinit var artifactService1: ArtifactService

    private lateinit var remoteAppService0: RemoteAppService
    private lateinit var remoteAppService1: RemoteAppService

    @BeforeEach
    fun init() {
        TestApp.clear()

        val rabbitConnectionFactory = MockConnectionFactory()
        rabbitConnectionFactory.host = "localhost"
        rabbitConnectionFactory.username = "admin"
        rabbitConnectionFactory.password = "admin"

        val rabbitMqConn = RabbitMqConn(rabbitConnectionFactory)
        rabbitMqConn.waitUntilReady(2_000)

        firstApp = TestApp(FIRST_APP_NAME, rabbitMqConn)
        secondApp = TestApp(SECOND_APP_NAME, rabbitMqConn)

        artifactService0 = firstApp.services.artifactService
        artifactService1 = secondApp.services.artifactService

        remoteAppService0 = firstApp.services.remoteAppService
        remoteAppService1 = secondApp.services.remoteAppService
    }

    @Test
    fun loadLocalModulesTypesFromFirstAppTest() {
        assertEquals(1, firstApp.types.size)
        assertEquals("workflow", firstApp.types[0].getId())
    }

    @Test
    fun loadLocalModulesTypesFroSecondAppTest() {
        assertEquals(5, secondApp.types.size)
        assertTrue(
            secondApp.types.map { it.getId() }.containsAll(
                setOf(
                    "ui/dashboard", "ui/form", "ui/icon",
                    "notification/template", "notification/file"
                )
            )
        )
    }

    private fun getArtifactsDir(to: TestApp, from: TestApp): EcosFile {
        return to.services.remoteAppService.getArtifactsDir(
            from.instanceId,
            SourceKey("", ArtifactSourceType.APPLICATION),
            to.typesDir,
            Instant.now()
        )
    }

    @Test
    fun loadRemoteModulesWorkflowTest() {

        val modules0types0 = getArtifactsDir(firstApp, firstApp)

        for (type in firstApp.types) {
            val modules = firstApp.services.artifactService.readArtifacts(modules0types0, type)
            if (type.getId() == "workflow") {
                assertEquals(1, modules.size)
                assertEquals("flowable\$flowable-scan", (modules[0] as ObjectData).get("id").asText())
            } else {
                throw IllegalStateException("Unknown type: ${type.getId()}")
            }
        }
    }

    @Test
    fun handlersTest() {

        val modules0types0 = getArtifactsDir(firstApp, firstApp)
        val modules1types0 = getArtifactsDir(firstApp, secondApp)
        val modules0types1 = getArtifactsDir(secondApp, firstApp)
        val modules1types1 = getArtifactsDir(secondApp, secondApp)

        val workflowModules0 = mutableListOf<Workflow>()
        val dashboardModules0 = mutableListOf<Dashboard>()
        val formModules0 = mutableListOf<Form>()
        val iconModules0 = mutableListOf<BinArtifact>()
        val templateModules0 = mutableListOf<BinArtifact>()
        val fileModule0 = mutableListOf<FileArtifact>()

        val handlersService0 = firstApp.services.artifactHandlerService

        registerHandlers(
            handlersService0,
            workflowModules0,
            dashboardModules0,
            formModules0,
            iconModules0,
            templateModules0,
            fileModule0
        )

        val workflowModules1 = mutableListOf<Workflow>()
        val dashboardModules1 = mutableListOf<Dashboard>()
        val formModules1 = mutableListOf<Form>()
        val iconModules1 = mutableListOf<BinArtifact>()
        val templateModules1 = mutableListOf<BinArtifact>()
        val fileModule1 = mutableListOf<FileArtifact>()

        val handlersService1 = secondApp.services.artifactHandlerService

        registerHandlers(
            handlersService1,
            workflowModules1,
            dashboardModules1,
            formModules1,
            iconModules1,
            templateModules1,
            fileModule1
        )

        for (type in firstApp.types) {
            val modules0 = artifactService0.readArtifacts(modules0types0, type)
            for (module in modules0) {
                remoteAppService0.deployArtifact(firstApp.instanceId, type.getId(), artifactService0.writeArtifactAsBytes(type.getId(), module))
            }
            val modules1 = artifactService0.readArtifacts(modules1types0, type)
            for (module in modules1) {
                remoteAppService0.deployArtifact(firstApp.instanceId, type.getId(), artifactService0.writeArtifactAsBytes(type.getId(), module))
            }
        }

        for (type in secondApp.types) {
            val modules0 = artifactService0.readArtifacts(modules0types1, type)
            for (module in modules0) {
                remoteAppService0.deployArtifact(secondApp.instanceId, type.getId(), artifactService0.writeArtifactAsBytes(type.getId(), module))
            }
            val modules1 = artifactService0.readArtifacts(modules1types1, type)
            for (module in modules1) {
                remoteAppService0.deployArtifact(secondApp.instanceId, type.getId(), artifactService0.writeArtifactAsBytes(type.getId(), module))
            }
        }

        assertTrue(iconModules0.isEmpty())
        assertEquals(1, iconModules1.size)
        assertEquals("inner/some-file.json", iconModules1[0].path)
        assertEquals("meta-id", iconModules1[0].meta.get("id").asText())

        val contentFile = EcosStdFile(File("./src/test/resources/apps/first_0/ecos-app/module/ui/icon/inner/some-file.json"))
        assertArrayEquals(contentFile.readAsBytes(), iconModules1[0].data)

        assertEquals(2, workflowModules0.size)
        assertThat(workflowModules0.map { it.id }).containsExactlyInAnyOrder(
            "flowable\$flowable-scan",
            "flowable\$flowable-confirm"
        )
        assertTrue(formModules0.isEmpty())
        assertTrue(dashboardModules0.isEmpty())

        assertEquals(4, dashboardModules1.size)
        assertThat(dashboardModules1.map { it.id }).containsExactlyInAnyOrder(
            "test-dsahboard", "test-dsahboard2",
            "test-dsahboard3", "app1-test-dashboard"
        )
        assertEquals(2, formModules1.size)
        assertThat(formModules1.map { it.id }).containsExactlyInAnyOrder("first-test-form", "yaml-test-form")

        assertEquals(7, templateModules1.size)
        assertThat(templateModules1.map { it.path }).containsExactlyInAnyOrder(
            "test-notification-without-lang.html.ftl.zip",
            "test-notification.zip",
            "test-notification.zip",
            "inner-with-file-meta/inner-template.html.ftl.zip",
            "inner-with-file-meta/inner-lang-template.html.zip",
            "test-same-name/same-name.html.zip",
            "test-same-name-2/same-name.html.zip"
        )

        val status = remoteAppService0.getAppsStatus()
        assertEquals(2, status.size)
    }

    @Test
    fun readModulesContent() {

        val modules0types1 = getArtifactsDir(secondApp, firstApp)
        val types1 = secondApp.types

        for (type in types1) {
            val modules = artifactService0.readArtifacts(modules0types1, type)
            when {
                type.getId() == "ui/dashboard" -> {
                    assertEquals(3, modules.size)
                    assertTrue(
                        modules.map { (it as ObjectData).get("id").asText() }.containsAll(
                            setOf("test-dsahboard", "test-dsahboard2", "test-dsahboard3")
                        )
                    )
                }
                type.getId() == "ui/form" -> {
                    assertEquals(2, modules.size)
                    assertTrue(
                        modules.map { (it as ObjectData).get("id").asText() }.containsAll(
                            setOf("first-test-form", "yaml-test-form")
                        )
                    )
                }
                type.getId() == "ui/icon" -> {
                    assertEquals(1, modules.size)
                    val dto = modules[0] as BinArtifact
                    assertEquals("value", dto.meta.get("/props/key", ""))
                    assertEquals("value1", dto.meta.get("/props/key1", ""))
                    assertEquals("some-file.json", dto.meta.get("/props/key2", ""))
                    assertEquals("some-file-json", dto.meta.get("/props/baseFilename", ""))
                    assertEquals("some-file", dto.meta.get("/props/arr/0", ""))
                    assertEquals("inner/some-file.json", dto.meta.get("/props/filePath", ""))
                }
                type.getId() == "notification/template" -> {
                    assertEquals(1, modules.size)
                    val dto = modules[0] as BinArtifact

                    val foundTemplateContents = mutableListOf<String>()
                    val dirFileName = mutableListOf<String>()

                    val dir = ZipUtils.extractZip(dto.data)
                    dir.findFiles().forEach { file ->
                        foundTemplateContents.add(file.readAsString().trim())
                        dirFileName.add(file.getName())
                    }

                    assertThat(dto.path).isEqualTo("test-notification.zip")
                    assertThat(foundTemplateContents).containsExactlyInAnyOrder("\${itsEn}", "Test content ru template")
                    assertThat(dirFileName).containsExactlyInAnyOrder("test-notification_en.ftl", "test-notification_ru.ftl")

                    assertEquals("Its title", dto.meta.get("/props/title", ""))
                    assertEquals("prop", dto.meta.get("/props/testProp", ""))
                    assertEquals("ftl", dto.meta.get("/props/ext", ""))
                }
                type.getId() == "notification/file" -> {
                    assertEquals(2, modules.size)

                    val paths = modules.stream().map { it as FileArtifact }.map { it.path }.toList()

                    assertThat(paths).containsExactlyInAnyOrder(
                        "w3c_home.gif",
                        "w3c_home.jpg"
                    )
                }
                else -> {
                    throw IllegalStateException("Unknown type: ${type.getId()}")
                }
            }
        }

        val modules1types0 = getArtifactsDir(firstApp, secondApp)
        val types0 = firstApp.types

        for (type in types0) {
            val modules = artifactService0.readArtifacts(modules1types0, type)
            if (type.getId() == "workflow") {
                assertEquals(1, modules.size)
                assertEquals("flowable\$flowable-confirm", (modules[0] as ObjectData).get("id").asText())
            } else {
                throw IllegalStateException("Unknown type: ${type.getId()}")
            }
        }

        val modules1types1 = getArtifactsDir(secondApp, secondApp)
        for (type in types1) {
            val modules = artifactService0.readArtifacts(modules1types1, type)
            when {
                type.getId() == "ui/dashboard" -> {
                    assertEquals(1, modules.size)
                    assertEquals("app1-test-dashboard", (modules[0] as ObjectData).get("id").asText())
                }
                type.getId() == "ui/form" -> {
                    assertEquals(0, modules.size)
                }
                type.getId() == "ui/icon" -> {
                    assertEquals(0, modules.size)
                }
                type.getId() == "notification/template" -> {
                    assertEquals(6, modules.size)

                    val paths = modules.stream().map { it as BinArtifact }.map { it.path }.toList()

                    assertThat(paths).containsExactlyInAnyOrder(
                        "test-notification-without-lang.html.ftl.zip",
                        "test-notification.zip",
                        "inner-with-file-meta/inner-template.html.ftl.zip",
                        "inner-with-file-meta/inner-lang-template.html.zip",
                        "test-same-name/same-name.html.zip",
                        "test-same-name-2/same-name.html.zip"
                    )

                    @Suppress("UNCHECKED_CAST")
                    val binModules = modules as List<BinArtifact>
                    assertThat(binModules.find { it.path == "test-notification.zip" }).isNotNull
                    assertThat(binModules.find { it.path == "test-notification-without-lang.html.ftl.zip" }).isNotNull

                    binModules.forEach {
                        when (it.path) {
                            "test-notification-without-lang.html.ftl.zip", "test-notification.ftl.zip" -> {
                                assertEquals("Its title 0", it.meta.get("/props/title", ""))
                                assertEquals("prop 0", it.meta.get("/props/testProp", ""))
                                assertEquals("ftl 0", it.meta.get("/props/ext", ""))
                            }

                            "inner-with-file-meta/inner-template.html.ftl.zip" -> {
                                assertEquals("title from inner with file meta", it.meta.get("/props/title", ""))
                                assertEquals("prop inner", it.meta.get("/props/testProp", ""))
                                assertEquals("ftl inner", it.meta.get("/props/ext", ""))
                            }

                            "inner-with-file-meta/inner-lang-template.html.zip" -> {
                                assertEquals("value from global meta", it.meta.get("/props/dirMeta", ""))
                                assertEquals("title from inner lang with file meta", it.meta.get("/props/title", ""))
                                assertEquals("prop inner lang", it.meta.get("/props/testProp", ""))
                                assertEquals("ftl inner lang", it.meta.get("/props/ext", ""))
                            }

                            "test-same-name/same-name.html.zip" -> {
                                assertEquals("folderValue1", it.meta.get("/folder-prop/folderKey1", ""))
                                assertEquals("folderValue2", it.meta.get("/folder-prop/folderKey2", ""))
                                assertEquals("value1", it.meta.get("/file-prop/key1", ""))
                                assertEquals("value2", it.meta.get("/file-prop/key2", ""))
                            }

                            "test-same-name-2/same-name.html.zip" -> {
                                assertEquals("folderValue3", it.meta.get("/folder-prop/folderKey3", ""))
                                assertEquals("folderValue4-overridden", it.meta.get("/folder-prop/folderKey4", ""))
                                assertEquals("value3", it.meta.get("/file-prop/key3", ""))
                                assertEquals("value4", it.meta.get("/file-prop/key4", ""))
                                assertEquals("value5-from-folder", it.meta.get("/file-prop/key5", ""))
                            }
                            "test-notification.zip" -> {
                            }
                            "test-notification-without-lang.html.zip" -> {
                            }

                            else -> throw java.lang.IllegalStateException("Unknown artifact path: ${it.path}")
                        }
                    }
                }
                type.getId() == "notification/file" -> {
                    assertEquals(6, modules.size)

                    val paths = modules.stream().map { it as FileArtifact }.map { it.path }.toList()

                    assertThat(paths).containsExactlyInAnyOrder(
                        "file_example_JPG_100kB.jpg",
                        "file_example_PNG_500kB.png",
                        "file_example_MP3_700KB.mp3",
                        "file_example_AVI_1280_1_5MG.avi",
                        "file-sample_150kB.pdf",
                        "file_example_XLS_50.xls"
                    )
                }
                else -> throw IllegalStateException("Unknown type: ${type.getId()}")
            }
        }
    }

    private fun registerHandlers(
        handlersService: ArtifactHandlerService,
        workflows: MutableList<Workflow>,
        dashboards: MutableList<Dashboard>,
        forms: MutableList<Form>,
        icons: MutableList<BinArtifact>,
        notifications: MutableList<BinArtifact>,
        files: MutableList<FileArtifact>
    ) {

        handlersService.register(object : SimpleArtifactHandler<Workflow>() {
            override fun deployArtifact(artifact: Workflow) {
                workflows.add(artifact)
            }
            override fun getArtifactType() = "workflow"
        })
        handlersService.register(object : SimpleArtifactHandler<Dashboard>() {
            override fun deployArtifact(artifact: Dashboard) {
                dashboards.add(artifact)
            }
            override fun getArtifactType() = "ui/dashboard"
        })
        handlersService.register(object : SimpleArtifactHandler<Form>() {
            override fun deployArtifact(artifact: Form) {
                forms.add(artifact)
            }
            override fun getArtifactType() = "ui/form"
        })
        handlersService.register(object : SimpleArtifactHandler<BinArtifact>() {
            override fun deployArtifact(artifact: BinArtifact) {
                icons.add(artifact)
            }
            override fun getArtifactType() = "ui/icon"
        })
        handlersService.register(object : SimpleArtifactHandler<BinArtifact>() {
            override fun deployArtifact(artifact: BinArtifact) {
                notifications.add(artifact)
            }
            override fun getArtifactType() = "notification/template"
        })
        handlersService.register(object : SimpleArtifactHandler<FileArtifact>() {
            override fun deployArtifact(artifact: FileArtifact) {
                files.add(artifact)
            }
            override fun getArtifactType() = "notification/file"
        })
    }

    data class Workflow(
        val id: String,
        val xmlData: ByteArray
    ) {
        override fun equals(other: Any?): Boolean {
            if (this === other) {
                return true
            }
            if (javaClass != other?.javaClass) {
                return false
            }
            other as Workflow
            if (id != other.id) {
                return false
            }
            if (!xmlData.contentEquals(other.xmlData)) {
                return false
            }
            return true
        }

        override fun hashCode(): Int {
            var result = id.hashCode()
            result = 31 * result + xmlData.contentHashCode()
            return result
        }
    }

    data class Form(
        val id: String,
        val title: String,
        val definition: ObjectData
    )

    data class Dashboard(
        val id: String,
        val key: String,
        val config: Map<String, Any>
    )

    abstract class SimpleArtifactHandler<T : Any> : EcosArtifactHandler<T> {
        override fun deleteArtifact(artifactId: String) {}
        override fun listenChanges(listener: Consumer<T>) {}
    }
}
