package ru.citeck.ecos.apps.artifact.controller.type

import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.io.file.filter.CompositeFileFilter
import ru.citeck.ecos.commons.io.file.filter.FileFilter
import ru.citeck.ecos.commons.io.file.filter.PathMatcherFilter
import java.nio.file.Path

object TypeControllerUtils {

    fun createFileFilter(base: Path, includes: List<String>, excludes: List<String>): FileFilter {

        return CompositeFileFilter(
            filters = listOf(
                CompositeFileFilter(
                    filters = includes.map { PathMatcherFilter(base, it) },
                    joinBy = CompositeFileFilter.JoinBy.OR
                ),
                NotFilter(
                    CompositeFileFilter(
                        filters = excludes.map { PathMatcherFilter(base, it) },
                        joinBy = CompositeFileFilter.JoinBy.OR
                    )
                )
            ),
            joinBy = CompositeFileFilter.JoinBy.AND
        )
    }

    class NotFilter(private val filter: FileFilter) : FileFilter {

        override fun accept(file: EcosFile): Boolean {
            return !filter.accept(file)
        }
    }
}
