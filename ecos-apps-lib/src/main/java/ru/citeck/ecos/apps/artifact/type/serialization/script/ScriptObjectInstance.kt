package ru.citeck.ecos.apps.artifact.type.serialization.script

import ru.citeck.ecos.commons.utils.ReflectUtils
import java.util.concurrent.ConcurrentHashMap

class ScriptObjectInstance<T : Any>(val instance: T) {

    private val genericsCache = ConcurrentHashMap<Class<*>, List<Class<*>>>()

    fun getGenericTypes(type: Class<*>): List<Class<*>> {
        return genericsCache.computeIfAbsent(type) {
            ReflectUtils.getGenericArgs(instance::class.java, type)
        }
    }
}
