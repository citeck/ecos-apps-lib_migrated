package ru.citeck.ecos.apps.artifact.controller.patch

import ru.citeck.ecos.commons.data.ObjectData

data class ArtifactPatch(
    val order: Float = 0f,
    val type: String,
    val config: ObjectData
)
