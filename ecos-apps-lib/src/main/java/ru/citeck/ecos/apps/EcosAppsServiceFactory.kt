package ru.citeck.ecos.apps

import ru.citeck.ecos.apps.app.api.*
import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceProvider
import ru.citeck.ecos.apps.app.domain.buildinfo.service.BuildInfoService
import ru.citeck.ecos.apps.app.domain.ecostype.service.ModelTypeArtifactService
import ru.citeck.ecos.apps.app.domain.handler.ArtifactHandlerService
import ru.citeck.ecos.apps.app.service.LocalAppService
import ru.citeck.ecos.apps.app.service.remote.RemoteAppService
import ru.citeck.ecos.apps.artifact.ArtifactService
import ru.citeck.ecos.apps.artifact.controller.ArtifactControllerService
import ru.citeck.ecos.apps.artifact.controller.type.JsonArtifactController
import ru.citeck.ecos.apps.artifact.controller.type.ScriptArtifactController
import ru.citeck.ecos.apps.artifact.controller.type.binary.BinArtifactController
import ru.citeck.ecos.apps.artifact.controller.type.file.FileArtifactController
import ru.citeck.ecos.apps.artifact.type.ArtifactTypeService
import ru.citeck.ecos.apps.eapps.api.ArtifactsForceUpdateCommandExecutor
import ru.citeck.ecos.apps.eapps.api.UploadArtifactCommandExecutor
import ru.citeck.ecos.apps.eapps.service.LocalEappsService
import ru.citeck.ecos.apps.eapps.service.RemoteEappsService
import ru.citeck.ecos.commands.CommandsServiceFactory
import ru.citeck.ecos.records3.RecordsServiceFactory

open class EcosAppsServiceFactory {

    // app

    val buildInfoService by lazy { createBuildInfoService() }

    val localAppService by lazy { createLocalAppService() }
    val remoteAppService by lazy { createRemoteAppService() }

    val ecosTypeArtifactService by lazy { createModelTypeArtifactsService() }
    val artifactHandlerService by lazy { createArtifactHandlerService() }

    // eapps

    val remoteEappsService by lazy { createRemoteEappsService() }
    val localEappsService by lazy { createLocalEappsService() }

    // artifact

    val artifactService by lazy { createArtifactService() }
    val artifactTypeService by lazy { createArtifactTypeService() }
    val artifactControllerService by lazy { createArtifactControllerService() }
    val artifactSourceProviders by lazy { createArtifactSourceProviders() }

    // external

    lateinit var recordsServices: RecordsServiceFactory
    lateinit var commandsServices: CommandsServiceFactory
    var properties = EcosAppsProperties()

    open fun init() {

        val commandsService = commandsServices.commandsService

        commandsService.addExecutor(DeployArtifactCommandExecutor(this))

        commandsService.addExecutor(GetAppStatusCommandExecutor(this))
        commandsService.addExecutor(GetArtifactsDirCommandExecutor(this))
        commandsService.addExecutor(GetArtifactTypesDirCommandExecutor(this))
        commandsService.addExecutor(GetModelTypeArtifactsCommandExecutor(this))
        commandsService.addExecutor(ArtifactsForceUpdateCommandExecutor(this))
        commandsService.addExecutor(GetAppBuildInfoCommandExecutor(this))

        commandsService.addExecutor(UploadArtifactCommandExecutor(this))
        commandsService.addExecutor(DeleteArtifactCommandExecutor(this))
    }

    protected open fun createBuildInfoService(): BuildInfoService {
        return BuildInfoService()
    }

    protected open fun createArtifactTypeService(): ArtifactTypeService {
        return ArtifactTypeService()
    }

    protected open fun createLocalEappsService(): LocalEappsService {
        return LocalEappsService()
    }

    protected open fun createArtifactHandlerService(): ArtifactHandlerService {
        return ArtifactHandlerService(this)
    }

    protected open fun createModelTypeArtifactsService(): ModelTypeArtifactService {
        return ModelTypeArtifactService()
    }

    protected open fun createRemoteAppService(): RemoteAppService {
        return RemoteAppService(this)
    }

    protected open fun createRemoteEappsService(): RemoteEappsService {
        val service = RemoteEappsService(this)
        artifactSourceProviders.forEach { provider ->
            provider.listenChanges { service.artifactsForceUpdate(it) }
        }
        return service
    }

    protected open fun createLocalAppService(): LocalAppService {
        return LocalAppService(this)
    }

    protected open fun createArtifactService(): ArtifactService {
        return ArtifactService(this)
    }

    protected open fun createArtifactControllerService(): ArtifactControllerService {
        val service = ArtifactControllerService()
        service.register("json", JsonArtifactController(this))
        service.register("script", ScriptArtifactController())
        service.register("bin", BinArtifactController())
        service.register("file", FileArtifactController())
        return service
    }

    protected open fun createArtifactSourceProviders(): List<ArtifactSourceProvider> {
        return emptyList()
    }
}
