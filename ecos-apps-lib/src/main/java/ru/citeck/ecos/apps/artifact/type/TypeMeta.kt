package ru.citeck.ecos.apps.artifact.type

import ru.citeck.ecos.apps.artifact.controller.ArtifactControllerDef
import ru.citeck.ecos.commons.data.MLText
import ru.citeck.ecos.commons.data.Version

data class TypeMeta(
    val name: MLText,
    val modelVersion: Version,
    val sourceId: String,
    val internal: Boolean,
    val controller: ArtifactControllerDef
)
