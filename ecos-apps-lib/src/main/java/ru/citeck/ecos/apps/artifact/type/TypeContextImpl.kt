package ru.citeck.ecos.apps.artifact.type

import ecos.com.fasterxml.jackson210.annotation.JsonProperty
import ecos.org.snakeyaml.engine21.v2.api.Load
import ecos.org.snakeyaml.engine21.v2.api.LoadSettings
import ru.citeck.ecos.apps.artifact.controller.ArtifactControllerDef
import ru.citeck.ecos.apps.artifact.type.serialization.EcosFileDeserializer
import ru.citeck.ecos.apps.artifact.type.serialization.script.EcosScriptExecutor
import ru.citeck.ecos.apps.artifact.type.serialization.script.ScriptObjectInstanceDeserializer
import ru.citeck.ecos.commons.data.MLText
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.data.Version
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.io.file.mem.EcosMemDir
import ru.citeck.ecos.commons.json.DeserFeature
import ru.citeck.ecos.commons.json.Json
import ru.citeck.ecos.commons.json.JsonOptions
import java.util.concurrent.ConcurrentHashMap

class TypeContextImpl(
    private val id: String,
    private val metaFile: EcosFile,
    private val scriptExecutor: EcosScriptExecutor
) : TypeContext {

    private val artifactRwConfigMapper = Json.newMapper(
        JsonOptions.create {
            add(EcosFileDeserializer(metaFile.getParent()!!))
            add(ScriptObjectInstanceDeserializer(metaFile.getParent()!!, scriptExecutor))
            disable(DeserFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        }
    )

    private val meta: TypeMeta
    private val content: EcosMemDir

    private val controllerConfigByType = ConcurrentHashMap<Class<*>, Any>()

    init {

        val parentDir = metaFile.getParent() ?: error("Parent not found. Path: ${metaFile.getPath()}")

        val yaml = Load(LoadSettings.builder().build())
        val fileMeta = metaFile.read {
            Json.mapper.convert(yaml.loadFromInputStream(it), TypeMetaFileDef::class.java)
        }!!

        meta = TypeMeta(
            fileMeta.name,
            fileMeta.modelVersion,
            fileMeta.sourceId,
            fileMeta.internal,
            fileMeta.controller
        )

        content = EcosMemDir()
        content.copyFilesFrom(parentDir)
    }

    override fun getId(): String {
        return id
    }

    override fun getMeta(): TypeMeta {
        return meta
    }

    override fun getContent(): EcosFile {
        return content
    }

    private fun <T : Any> getConfig(data: ObjectData?, type: Class<T>): T {
        return if (data == null) {
            artifactRwConfigMapper.convert(ObjectData.create(), type)!!
        } else {
            artifactRwConfigMapper.convert(data, type)!!
        }
    }

    override fun getControllerType() = meta.controller.type

    override fun <T : Any> getControllerConfig(type: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return controllerConfigByType.computeIfAbsent(type) { getConfig(meta.controller.config, type) } as T
    }

    override fun getControllerConfig(): ObjectData {
        return meta.controller.config
    }

    data class TypeMetaFileDef(
        var modelVersion: Version = Version("1.0"),
        var sourceId: String = "",
        var name: MLText = MLText(),
        val controller: ArtifactControllerDef,
        val internal: Boolean = false
    ) {

        @JsonProperty("model-version")
        fun setModelVersionWithDash(value: Version) {
            this.modelVersion = value
        }

        @JsonProperty("source-id")
        fun setSourceIdWithDash(value: String) {
            this.sourceId = value
        }
    }
}
