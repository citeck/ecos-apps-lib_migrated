package ru.citeck.ecos.apps.artifact.controller.type.binary

import mu.KotlinLogging
import ru.citeck.ecos.apps.artifact.ArtifactMeta
import ru.citeck.ecos.apps.artifact.controller.ArtifactController
import ru.citeck.ecos.apps.artifact.controller.type.TypeControllerUtils
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.data.MLText
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.io.file.mem.EcosMemDir
import ru.citeck.ecos.commons.json.Json
import ru.citeck.ecos.commons.utils.ZipUtils
import java.io.ByteArrayInputStream
import java.nio.file.Path
import java.util.*
import java.util.regex.Pattern
import java.util.stream.Collectors

class BinArtifactController : ArtifactController<BinArtifact, BinArtifactController.Config> {

    companion object {

        val log = KotlinLogging.logger {}

        const val DIR_META = "meta.yml"

        const val FILE_PATH_PLACEHOLDER = "\${filePath}"
        const val FILE_NAME_PLACEHOLDER = "\${fileName}"
        const val FILE_BASE_NAME_PLACEHOLDER = "\${fileNameBase}"
        const val FILE_EXTENSION_PLACEHOLDER = "\${fileNameExt}"
    }

    override fun read(root: EcosFile, config: Config): List<BinArtifact> {

        val excludes = listOf(*config.excludes.toTypedArray(), "**.meta.yml", DIR_META, "**/$DIR_META")
        val fileFilter = TypeControllerUtils.createFileFilter(root.getPath(), config.includes, excludes)

        val dirMetaCache = HashMap<Path, ObjectData>()

        val foundArtifacts = root.findFiles(fileFilter)
            .stream()
            .map { ecosFile ->
                Optional.ofNullable(
                    try {
                        val artifact = this.readArtifact(root, ecosFile, config, dirMetaCache)
                        artifact?.let { Pair(artifact.path, artifact) }
                    } catch (e: Exception) {
                        log.error(e) { "Binary artifact reading failed. File: ${ecosFile.getPath()}" }
                        null
                    }
                )
            }
            .filter { it.isPresent }
            .map { it.get() }
            .collect(Collectors.toList())

        return if (!config.artifactNamePattern.isNullOrBlank()) {
            groupingArtifactsByName(foundArtifacts, config)
        } else {
            foundArtifacts.stream()
                .map {
                    it.second
                }
                .collect(Collectors.toList())
        }
    }

    private fun readArtifact(
        root: EcosFile,
        artifact: EcosFile,
        config: Config,
        dirMetaCache: MutableMap<Path, ObjectData>
    ): BinArtifact? {

        val dirMetaFile = artifact.getParent()?.getFile(DIR_META)
        val dirMeta = if (dirMetaFile != null) {
            dirMetaCache.computeIfAbsent(dirMetaFile.getPath()) {
                Json.mapper.read(dirMetaFile, ObjectData::class.java) ?: ObjectData.create()
            }
        } else {
            ObjectData.create()
        }

        val path = root.getPath()
            .relativize(artifact.getPath())
            .toString()
            .replace("\\", "/")

        var metaFileName = replacePlaceholders(config.metaFile, artifact, path)
        val artifactNamePattern = config.artifactNamePattern
        if (!artifactNamePattern.isNullOrBlank()) {
            val patternComp = Pattern.compile(artifactNamePattern)
            val matcher = patternComp.matcher(metaFileName)
            if (matcher.find()) {
                val fileName = matcher.group(config.artifactNameGroupIdx)
                metaFileName = config.metaFile.replace(FILE_NAME_PLACEHOLDER, fileName)
            }
        }

        val metaFile = if (metaFileName.isNotBlank()) {
            artifact.getParent()?.getFile(metaFileName)
        } else {
            null
        }
        if (config.metaRequired && metaFile == null && dirMeta.size() == 0) {
            return null
        }

        val fileMeta = Json.mapper.read(metaFile, ObjectData::class.java) ?: ObjectData.create()
        var meta = merge(fileMeta.deepCopy().getData(), dirMeta.getData())
        meta = replacePlaceholders(meta, artifact, path)

        return BinArtifact(path, meta.asObjectData(), artifact.readAsBytes())
    }

    private fun merge(target: DataValue, from: DataValue): DataValue {
        if (target.isNull()) {
            return from
        }
        if (from.isObject() && target.isObject()) {
            from.forEach { k, v ->
                target.set(k, merge(target.get(k), v))
            }
        }
        return target
    }

    private fun replacePlaceholders(value: DataValue, artifact: EcosFile, path: String): DataValue {
        when {
            value.isTextual() -> {
                return DataValue.createStr(replacePlaceholders(value.asText(), artifact, path))
            }
            value.isObject() -> {
                val res = DataValue.createObj()
                value.forEach { k, v ->
                    res.set(k, replacePlaceholders(v, artifact, path))
                }
                return res
            }
            value.isArray() -> {
                val res = DataValue.createArr()
                value.forEach { v ->
                    res.add(replacePlaceholders(v, artifact, path))
                }
                return res
            }
            else -> return value
        }
    }

    private fun replacePlaceholders(value: String, artifact: EcosFile, path: String): String {

        var text = value

        val fileName = artifact.getName()
        val extDotIdx = fileName.lastIndexOf('.')
        val hasExtension = extDotIdx > 0 && extDotIdx < fileName.length - 1

        text = text.replace(FILE_NAME_PLACEHOLDER, fileName)
        text = text.replace(FILE_PATH_PLACEHOLDER, path)

        if (text.contains(FILE_BASE_NAME_PLACEHOLDER)) {
            val baseName = if (hasExtension) {
                fileName.substring(0, extDotIdx)
            } else {
                fileName
            }
            text = text.replace(FILE_BASE_NAME_PLACEHOLDER, baseName)
        }
        if (text.contains(FILE_EXTENSION_PLACEHOLDER)) {
            val extension = if (hasExtension) {
                fileName.substring(extDotIdx + 1)
            } else {
                fileName
            }
            text = text.replace(FILE_EXTENSION_PLACEHOLDER, extension)
        }
        return text
    }

    private fun groupingArtifactsByName(allFoundArtifacts: List<Pair<String, BinArtifact>>, config: Config): List<BinArtifact> {

        val artifactNamePattern = config.artifactNamePattern
        if (artifactNamePattern.isNullOrBlank()) {
            return emptyList()
        }

        val patternComp = Pattern.compile(artifactNamePattern)

        val toMerge: MutableMap<String, MutableList<BinArtifact>> = mutableMapOf()
        val merged: MutableList<BinArtifact> = mutableListOf()

        allFoundArtifacts.forEach { nameArtifactPair ->
            val originalFilePath = nameArtifactPair.first
            val artifact = nameArtifactPair.second

            val matcher = patternComp.matcher(originalFilePath)
            if (matcher.find()) {
                var mergedPath = matcher.group(config.artifactNameGroupIdx)
                if (!mergedPath.endsWith(".zip")) {
                    mergedPath += ".zip"
                }

                toMerge.computeIfAbsent(mergedPath) { mutableListOf() }.add(artifact)
            } else {
                if (dataAlreadyZipped(artifact.data)) {
                    merged.add(artifact)
                } else {
                    merged.add(zipBinArtifactDataAsFile(artifact))
                }
            }
        }

        toMerge.forEach { (groupPathName, artifacts) ->
            merged.add(mergeArtifactsDataInZip(groupPathName, artifacts))
        }

        return merged
    }

    private fun dataAlreadyZipped(bytes: ByteArray): Boolean {
        return ZipUtils.extractZip(bytes).getChildren().isNotEmpty()
    }

    private fun zipBinArtifactDataAsFile(artifact: BinArtifact): BinArtifact {

        val dir = EcosMemDir()
        dir.createFile(getFileNameFromPath(artifact.path), artifact.data)

        val filePath = if (!artifact.path.endsWith(".zip")) {
            artifact.path + ".zip"
        } else {
            artifact.path
        }

        return BinArtifact(
            filePath,
            artifact.meta,
            ZipUtils.writeZipAsBytes(dir)
        )
    }

    private fun mergeArtifactsDataInZip(artifactName: String, artifacts: List<BinArtifact>): BinArtifact {
        val firstArtifact: BinArtifact = artifacts.first()

        val dir = EcosMemDir()
        artifacts.forEach {
            dir.createFile(getFileNameFromPath(it.path), it.data)
        }

        return BinArtifact(artifactName, firstArtifact.meta, ZipUtils.writeZipAsBytes(dir))
    }

    override fun write(root: EcosFile, artifact: BinArtifact, config: Config) {

        val baseName = if (artifact.path.endsWith(".zip")) {

            val pathDelimIdx = artifact.path.lastIndexOf('/')
            val dirToExtract = if (pathDelimIdx > 0) {
                root.getOrCreateDir(artifact.path.substring(0, pathDelimIdx))
            } else {
                root
            }

            ZipUtils.extractZip(ByteArrayInputStream(artifact.data), dirToExtract)
            getFileNameFromPath(artifact.path).substringBeforeLast('.')
        } else {

            val artifactFile = root.createFile(artifact.path) { output ->
                output.write(artifact.data)
            }
            artifactFile.getName()
        }

        if (config.metaRequired || artifact.meta.size() > 0) {

            val metaFilename = config.metaFile.replace("\${fileName}", baseName)

            val lastSlashIdx = artifact.path.indexOfLast { it == '/' }
            val fullMetaPath = if (lastSlashIdx > 0) {
                artifact.path.substring(0, lastSlashIdx) + "/" + metaFilename
            } else {
                metaFilename
            }
            root.createFile(fullMetaPath) { Json.mapper.write(it, artifact.meta) }
        }
    }

    override fun getMeta(artifact: BinArtifact, config: Config): ArtifactMeta {
        var id = artifact.meta.get("id").asText()
        if (id.isBlank()) {
            id = artifact.path
        }
        val nameValue = artifact.meta.get("name")
        val name: MLText
        if (nameValue.isObject()) {
            name = nameValue.getAs(MLText::class.java) ?: MLText.EMPTY
        } else if (nameValue.isTextual()) {
            name = MLText(nameValue.asText())
        } else {
            name = MLText(id)
        }
        return ArtifactMeta.create {
            withId(id)
            withName(name)
        }
    }

    private fun getFileNameFromPath(path: String): String {
        val dirDelimIdx = path.lastIndexOf('/')
        return if (dirDelimIdx > 0 && dirDelimIdx < path.length - 2) {
            path.substring(dirDelimIdx + 1)
        } else {
            path
        }
    }

    data class Config(
        val includes: List<String> = listOf("**.{json,yml,yaml,xml,png,ftl,html}"),
        val excludes: List<String> = emptyList(),
        val metaFile: String = "\${fileName}.meta.yml",
        val metaRequired: Boolean = false,
        var artifactNamePattern: String? = null,
        var artifactNameGroupIdx: Int = 1
    ) {

        fun setModuleNamePattern(name: String) {
            this.artifactNamePattern = name
        }

        fun setModuleNameGroupIdx(idx: Int) {
            this.artifactNameGroupIdx = idx
        }
    }
}
