package ru.citeck.ecos.apps.eapps.service

import ru.citeck.ecos.apps.EcosAppsConstants
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceInfo
import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceType
import ru.citeck.ecos.apps.app.domain.artifact.source.SourceKey
import ru.citeck.ecos.apps.artifact.ArtifactService
import ru.citeck.ecos.apps.eapps.api.ArtifactsForceUpdateCommand
import ru.citeck.ecos.apps.eapps.api.UploadArtifactCommand
import ru.citeck.ecos.commands.CommandsService
import java.time.Duration

class RemoteEappsService(services: EcosAppsServiceFactory) {

    private val artifactService: ArtifactService = services.artifactService
    private val commands: CommandsService = services.commandsServices.commandsService
    private val commandCtxManager = services.commandsServices.commandCtxManager

    fun fireArtifactChangedByUser(type: String, artifact: Any) {

        val artifactBytes = artifactService.writeArtifactAsBytes(type, artifact)
        var currentUser = commandCtxManager.getCurrentUser()

        if (currentUser.isBlank()) {
            currentUser = "anonymous"
        }

        commands.execute {
            targetApp = EcosAppsConstants.EAPPS_SERVICE_NAME
            body = UploadArtifactCommand(type, artifactBytes, SourceKey(currentUser, ArtifactSourceType.USER))
            ttl = Duration.ZERO
        }
    }

    fun artifactsForceUpdate(source: ArtifactSourceInfo) {

        commands.executeSync {
            targetApp = EcosAppsConstants.EAPPS_SERVICE_NAME
            body = ArtifactsForceUpdateCommand(source)
            ttl = Duration.ofSeconds(5)
        }
    }
}
