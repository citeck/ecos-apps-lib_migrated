package ru.citeck.ecos.apps.app.domain.buildinfo.dto

import ecos.com.fasterxml.jackson210.databind.annotation.JsonDeserialize
import ru.citeck.ecos.commons.data.MLText
import java.time.Instant
import com.fasterxml.jackson.databind.annotation.JsonDeserialize as JackDeserialize

@JsonDeserialize(builder = BuildInfo.Builder::class)
@JackDeserialize(builder = BuildInfo.Builder::class)
data class BuildInfo(
    val name: MLText,
    val repo: String,
    val branch: String,
    val version: String,
    val buildDate: Instant,
    val commits: List<CommitInfo>
) {

    companion object {

        @JvmStatic
        fun create(builder: Builder.() -> Unit): BuildInfo {
            return Builder().apply(builder).build()
        }
    }

    fun copy(): Builder {
        return Builder(this)
    }

    fun copy(builder: Builder.() -> Unit): BuildInfo {
        val builderObj = Builder(this)
        builder.invoke(builderObj)
        return builderObj.build()
    }

    class Builder() {

        var name: MLText = MLText.EMPTY
        var repo: String = ""
        var branch: String = ""
        var version: String = ""
        var buildDate: Instant = Instant.EPOCH
        var commits: List<CommitInfo> = emptyList()

        constructor(base: BuildInfo) : this() {
            this.name = base.name
            this.repo = base.repo
            this.branch = base.branch
            this.version = base.version
            this.buildDate = base.buildDate
            this.commits = base.commits
        }

        fun withName(name: MLText?): Builder {
            this.name = name ?: MLText.EMPTY
            return this
        }

        fun withRepo(repo: String?): Builder {
            this.repo = repo ?: ""
            return this
        }

        fun withBranch(branch: String?): Builder {
            this.branch = branch ?: ""
            return this
        }

        fun withVersion(version: String?): Builder {
            this.version = version ?: ""
            return this
        }

        fun withBuildDate(buildDate: Instant): Builder {
            this.buildDate = buildDate
            return this
        }

        fun withCommits(commits: List<CommitInfo>): Builder {
            this.commits = commits
            return this
        }

        fun build(): BuildInfo {
            return BuildInfo(name, repo, branch, version, buildDate, commits)
        }
    }
}
