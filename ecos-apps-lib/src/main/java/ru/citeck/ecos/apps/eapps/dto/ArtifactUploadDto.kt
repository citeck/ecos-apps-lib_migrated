package ru.citeck.ecos.apps.eapps.dto

import ru.citeck.ecos.apps.app.domain.artifact.source.AppSourceKey

data class ArtifactUploadDto(
    val type: String,
    val artifact: Any,
    val source: AppSourceKey
)
