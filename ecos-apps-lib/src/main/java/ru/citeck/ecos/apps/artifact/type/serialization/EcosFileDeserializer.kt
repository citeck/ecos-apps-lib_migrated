package ru.citeck.ecos.apps.artifact.type.serialization

import ecos.com.fasterxml.jackson210.core.JsonParser
import ecos.com.fasterxml.jackson210.databind.DeserializationContext
import ecos.com.fasterxml.jackson210.databind.deser.std.StdDeserializer
import ru.citeck.ecos.commons.io.file.EcosFile

class EcosFileDeserializer(val scope: EcosFile) : StdDeserializer<EcosFile>(EcosFile::class.java) {

    override fun deserialize(parser: JsonParser, ctx: DeserializationContext?): EcosFile {
        val path = parser.valueAsString
        return scope.getFile(path)!!
    }
}
