package ru.citeck.ecos.apps.artifact.type.serialization.script

import groovy.util.Eval
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.utils.digest.Digest
import ru.citeck.ecos.commons.utils.digest.DigestAlgorithm
import ru.citeck.ecos.commons.utils.digest.DigestUtils
import java.lang.IllegalArgumentException
import java.util.concurrent.ConcurrentHashMap

class EcosScriptExecutor {

    private val scriptObjects = ConcurrentHashMap<Digest, Any>()

    fun <T : Any> getObject(scriptFile: EcosFile): T {
        val fileContent = scriptFile.readAsString()
        val digest = DigestUtils.getDigest(fileContent.toByteArray(Charsets.UTF_8), DigestAlgorithm.SHA256)
        @Suppress("UNCHECKED_CAST")
        return scriptObjects.computeIfAbsent(digest) {
            if (scriptFile.getName().endsWith(".groovy")) {
                Eval.me(fileContent)
            } else {
                throw IllegalArgumentException("Unknown script type: ${scriptFile.getName()}")
            }
        } as T
    }
}
