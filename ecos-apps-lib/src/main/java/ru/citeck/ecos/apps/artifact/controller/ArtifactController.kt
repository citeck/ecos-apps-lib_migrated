package ru.citeck.ecos.apps.artifact.controller

import ru.citeck.ecos.apps.artifact.ArtifactMeta
import ru.citeck.ecos.commons.io.file.EcosFile

interface ArtifactController<T : Any, C : Any> {

    fun read(root: EcosFile, config: C): List<T>

    fun write(root: EcosFile, artifact: T, config: C)

    fun getMeta(artifact: T, config: C): ArtifactMeta
}
