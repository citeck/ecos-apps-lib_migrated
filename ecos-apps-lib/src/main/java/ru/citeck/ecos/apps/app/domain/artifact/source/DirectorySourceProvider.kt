package ru.citeck.ecos.apps.app.domain.artifact.source

import ru.citeck.ecos.apps.app.domain.artifact.reader.ArtifactsReader
import ru.citeck.ecos.apps.artifact.type.TypeContext
import ru.citeck.ecos.commons.io.file.EcosFile
import java.time.Instant

class DirectorySourceProvider(
    private val directory: EcosFile
) : ArtifactSourceProvider {

    companion object {
        private const val CLASSPATH_SOURCE_ID = "classpath"
    }

    private lateinit var reader: ArtifactsReader

    override fun init(reader: ArtifactsReader) {
        this.reader = reader
    }

    override fun getArtifactSources(): List<ArtifactSourceInfo> {
        return listOf(
            ArtifactSourceInfo.create {
                withKey(SourceKey(CLASSPATH_SOURCE_ID, ArtifactSourceType.APPLICATION))
            }
        )
    }

    override fun getArtifacts(source: SourceKey, types: List<TypeContext>, since: Instant): Map<String, List<Any>> {
        if (CLASSPATH_SOURCE_ID != source.id) {
            return emptyMap()
        }
        return reader.readArtifacts(directory, types)
    }

    override fun listenChanges(listener: (ArtifactSourceInfo) -> Unit) {
    }

    override fun isStatic(): Boolean {
        return true
    }
}
