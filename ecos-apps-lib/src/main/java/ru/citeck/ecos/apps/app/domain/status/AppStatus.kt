package ru.citeck.ecos.apps.app.domain.status

import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceInfo
import java.time.Instant

data class AppStatus(
    val typesLastModified: Instant,
    val supportedTypes: List<String>,
    val sources: List<ArtifactSourceInfo> = emptyList(),
    val startTime: Instant = Instant.EPOCH
)
