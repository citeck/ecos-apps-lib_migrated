package ru.citeck.ecos.apps.app.domain.buildinfo.dto

import ecos.com.fasterxml.jackson210.databind.annotation.JsonDeserialize
import java.time.Instant
import com.fasterxml.jackson.databind.annotation.JsonDeserialize as JackDeserialize

@JsonDeserialize(builder = CommitPersonInfo.Builder::class)
@JackDeserialize(builder = CommitPersonInfo.Builder::class)
data class CommitPersonInfo(
    val name: String,
    val email: String,
    val date: Instant
) {
    companion object {

        @JvmField
        val EMPTY = create {}

        @JvmStatic
        fun create(builder: Builder.() -> Unit): CommitPersonInfo {
            return Builder().apply(builder).build()
        }
    }

    fun copy(): Builder {
        return Builder(this)
    }

    fun copy(builder: Builder.() -> Unit): CommitPersonInfo {
        val builderObj = Builder(this)
        builder.invoke(builderObj)
        return builderObj.build()
    }

    class Builder() {

        var name: String = ""
        var email: String = ""
        var date: Instant = Instant.EPOCH

        constructor(base: CommitPersonInfo) : this() {
            this.name = base.name
            this.email = base.email
            this.date = base.date
        }

        fun withName(name: String?): Builder {
            this.name = name ?: ""
            return this
        }

        fun withEmail(email: String?): Builder {
            this.email = email ?: ""
            return this
        }

        fun withDate(date: Instant?): Builder {
            this.date = date ?: Instant.EPOCH
            return this
        }

        fun build(): CommitPersonInfo {
            return CommitPersonInfo(name, email, date)
        }
    }
}
