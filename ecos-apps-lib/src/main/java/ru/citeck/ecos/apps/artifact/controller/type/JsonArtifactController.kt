package ru.citeck.ecos.apps.artifact.controller.type

import ecos.com.fasterxml.jackson210.databind.JsonNode
import ecos.com.fasterxml.jackson210.databind.node.ObjectNode
import ecos.com.networknt.schema.JsonSchema
import ecos.com.networknt.schema.JsonSchemaFactory
import ecos.com.networknt.schema.SpecVersion
import mu.KotlinLogging
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.apps.artifact.ArtifactMeta
import ru.citeck.ecos.apps.artifact.controller.ArtifactController
import ru.citeck.ecos.apps.artifact.controller.patch.ArtifactPatch
import ru.citeck.ecos.apps.artifact.controller.patch.ArtifactPatchController
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.data.MLText
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.json.Json
import ru.citeck.ecos.commons.utils.TmplUtils
import ru.citeck.ecos.records2.RecordRef
import ru.citeck.ecos.records3.record.atts.dto.RecordAtts
import java.util.*
import java.util.stream.Collectors

open class JsonArtifactController(factory: EcosAppsServiceFactory) :
    ArtifactPatchController<ObjectData, JsonArtifactController.Config>,
    ArtifactController<ObjectData, JsonArtifactController.Config> {

    companion object {
        private val log = KotlinLogging.logger {}
    }

    private val recordsService = factory.recordsServices.recordsServiceV1

    override fun read(root: EcosFile, config: Config): List<ObjectData> {

        val schema = getSchema(config.schema)
        val fileFilter = TypeControllerUtils.createFileFilter(root.getPath(), config.includes, config.excludes)

        return root.findFiles(fileFilter)
            .stream()
            .map {
                Optional.ofNullable(
                    try {
                        val nameParts = it.getName().split(".")
                        if (nameParts.size >= 3 && nameParts[nameParts.size - 2] == "inc") {
                            null
                        } else {
                            var result = Json.mapper.read(it, ObjectData::class.java)
                            if (result == null) {
                                log.error { "Artifact reading failed. Result is null. File: ${it.getPath()}" }
                                null
                            } else {
                                validateData(result, schema)
                                if (config.attsValues.isNotEmpty()) {
                                    val newObj = result.deepCopy()
                                    config.attsValues.forEach { (key, value) ->
                                        if (value.isTextual()) {
                                            newObj.set(key, resolveRecTemplate(value.asText(), result))
                                        } else {
                                            newObj.set(key, value)
                                        }
                                    }
                                    result = newObj
                                }
                                result
                            }
                        }
                    } catch (e: Exception) {
                        log.error(e) { "Artifact reading failed: '${it.getPath()}'" }
                        null
                    }
                )
            }
            .filter { it.isPresent }
            .map { it.get() }
            .map { it as ObjectData }
            .collect(Collectors.toList())
    }

    override fun write(root: EcosFile, artifact: ObjectData, config: Config) {
        val id = artifact.get("id").asText()
        if (id.isBlank()) {
            log.warn { "Artifact doesn't have id. Skip it: '$artifact'" }
        } else {
            root.createFile("$id.json") { output ->
                Json.mapper.write(output, artifact)
            }
        }
    }

    override fun applyPatch(artifact: ObjectData, config: Config, patch: ArtifactPatch): ObjectData {

        if (patch.type != "json") {
            log.error("Patch type ${patch.type} is not supported! Patch: $patch")
            return artifact
        }

        val newArtifact = artifact.deepCopy()
        val operations = patch.config.get("operations").toList(DataValue::class.java)

        opLoop@ for (operation in operations) {

            log.info { "Apply patch operation for artifact: $operation" }
            val path = operation.get("path").asText()

            when (operation.get("op").asText()) {
                "add" -> {
                    if (!operation.has("value")) {
                        log.error("value is a mandatory parameter for 'add' operation. Patch: $patch")
                        continue@opLoop
                    }
                    val value = operation.get("value")
                    if (value.isArray()) {
                        if (operation.has("idx")) {
                            newArtifact.insertAll(path, operation.get("idx").asInt(), value)
                        } else {
                            newArtifact.addAll(path, value)
                        }
                    } else {
                        if (operation.has("idx")) {
                            newArtifact.insert(path, operation.get("idx").asInt(), value)
                        } else {
                            newArtifact.add(path, value)
                        }
                    }
                }
                "remove" -> {
                    newArtifact.remove(path)
                }
                "rename-key" -> {

                    val oldKey = operation.get("oldKey").asText()
                    val newKey = operation.get("newKey").asText()

                    if (newKey.isBlank() || oldKey.isBlank()) {
                        log.error("newKey and oldKey should be specified for 'rename-key' operation. Patch: $patch")
                        continue@opLoop
                    }
                    newArtifact.renameKey(path, oldKey, newKey)
                }
                "set" -> {
                    if (!operation.has("value")) {
                        log.error("value is a mandatory parameter for 'set' operation. Patch: $patch")
                        continue@opLoop
                    }
                    val key = operation.get("key").asText()
                    val value = operation.get("value")
                    if (key.isNotBlank()) {
                        newArtifact.set(path, key, value)
                    } else {
                        newArtifact.set(path, value)
                    }
                }
            }
        }

        return newArtifact
    }

    override fun getMeta(artifact: ObjectData, config: Config): ArtifactMeta {

        val metaFields = config.metaFields

        val dependenciesSet = mutableSetOf<String>()
        val dependencies = getStrAttsList(artifact, metaFields.dependencies)
            .filter { dependenciesSet.add(it) }
            .map { RecordRef.valueOf(it) }

        val tags = getStrAttsList(artifact, metaFields.tags)
        val name = getMLTextAtt(artifact, metaFields.name)

        val id = if (config.idTemplate.isBlank()) {
            recordsService.getAtt(artifact, metaFields.id).asText()
        } else {
            resolveRecTemplate(config.idTemplate, artifact)
        }
        val system = recordsService.getAtt(artifact, metaFields.system).asBoolean(false)

        if (id.isBlank()) {
            error("Artifact with empty id. Config: $config Artifact: $artifact")
        }

        return ArtifactMeta.create {
            withId(id)
            withDependencies(dependencies)
            if (name != null) {
                withName(name)
            }
            withTags(tags)
            withSystem(system)
        }
    }

    private fun resolveRecTemplate(template: String, value: Any?): String {
        if (!template.contains("$") || value == null) {
            return template
        }
        val atts: Set<String> = TmplUtils.getAtts(template)
        return if (atts.isEmpty()) {
            template
        } else {
            val meta: RecordAtts = recordsService.getAtts(value, atts)
            TmplUtils.applyAtts(template, meta.getAtts()).asText()
        }
    }

    private fun getMLTextAtt(artifact: ObjectData, attribute: String): MLText? {

        val mlAtt = if (attribute.contains("{") || attribute.contains("?")) {
            attribute
        } else {
            "$attribute?json"
        }
        val result = recordsService.getAtt(artifact, mlAtt)

        if (!result.isObject() || result.size() == 0) {
            return null
        }

        return Json.mapper.convert(result, MLText::class.java)
    }

    private fun getStrAttsList(artifact: ObjectData, attributes: List<String>): List<String> {

        val atts = recordsService.getAtts(artifact, attributes)
        val result = mutableListOf<String>()

        atts.forEach { _, value -> fillStrings(value, result) }

        return result
    }

    private fun fillStrings(value: DataValue, result: MutableList<String>) {

        if (value.isTextual()) {
            if (value.asText().isNotBlank()) {
                result.add(value.asText())
            }
        } else if (value.isArray()) {
            value.forEach { fillStrings(it, result) }
        }
    }

    private fun validateData(data: ObjectData?, schema: JsonSchema?) {

        if (schema == null || data == null) {
            return
        }
        val result = schema.validate(data.getAs(ObjectNode::class.java))

        result.forEach {
            log.warn {
                "$it. Args: " + Arrays.toString(it.arguments ?: emptyArray()) + " Details: " + it.details
            }
        }
    }

    private fun getSchema(file: EcosFile?): JsonSchema? {

        if (file == null) {
            return null
        }

        val schemaNode = Json.mapper.read(file, JsonNode::class.java)
        return if (schemaNode != null) {
            try {
                JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V7).getSchema(schemaNode)
            } catch (e: Exception) {
                log.error(e) { "Schema reading failed. Node: $schemaNode" }
                null
            }
        } else {
            null
        }
    }

    data class Config(
        val idTemplate: String = "",
        val includes: List<String> = listOf("**.{json,yml,yaml}"),
        val excludes: List<String> = emptyList(),
        val schema: EcosFile? = null,
        val metaFields: MetaFields = MetaFields(),
        val attsValues: Map<String, DataValue> = emptyMap()
    )

    data class MetaFields(
        val id: String = "id",
        val name: String = "name",
        val tags: List<String> = listOf("tags[]"),
        val dependencies: List<String> = listOf("dependencies[]"),
        val system: String = "system?bool"
    )
}
