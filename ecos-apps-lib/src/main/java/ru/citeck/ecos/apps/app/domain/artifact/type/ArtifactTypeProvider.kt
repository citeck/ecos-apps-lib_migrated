package ru.citeck.ecos.apps.app.domain.artifact.type

import ru.citeck.ecos.commons.io.file.EcosFile
import java.time.Instant

interface ArtifactTypeProvider {

    fun getArtifactTypesDir(): EcosFile

    fun getLastModified(): Instant?
}
