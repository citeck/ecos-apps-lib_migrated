package ru.citeck.ecos.apps.app.api

import ru.citeck.ecos.apps.EcosAppsConstants
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.commands.CommandExecutor
import ru.citeck.ecos.commands.annotation.CommandType

class DeployArtifactCommandExecutor(services: EcosAppsServiceFactory) : CommandExecutor<DeployArtifactCommand> {

    private val localAppService = services.localAppService

    override fun execute(command: DeployArtifactCommand): Any? {
        localAppService.deployArtifact(command.type, command.data)
        return true
    }
}

@CommandType("${EcosAppsConstants.COMMANDS_PREFIX_APP}.deploy-artifact")
class DeployArtifactCommand(
    val type: String,
    val data: ByteArray
)
