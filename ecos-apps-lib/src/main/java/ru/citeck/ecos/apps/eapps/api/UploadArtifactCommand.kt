package ru.citeck.ecos.apps.eapps.api

import ru.citeck.ecos.apps.EcosAppsConstants
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.apps.app.domain.artifact.source.AppSourceKey
import ru.citeck.ecos.apps.app.domain.artifact.source.SourceKey
import ru.citeck.ecos.apps.artifact.ArtifactService
import ru.citeck.ecos.apps.eapps.dto.ArtifactUploadDto
import ru.citeck.ecos.commands.CommandExecutor
import ru.citeck.ecos.commands.annotation.CommandType
import ru.citeck.ecos.commands.context.CommandCtxManager

class UploadArtifactCommandExecutor(services: EcosAppsServiceFactory) :
    CommandExecutor<UploadArtifactCommand> {

    private val localEappService = services.localEappsService
    private val artifactService: ArtifactService = services.artifactService
    private val commandCtxManager: CommandCtxManager = services.commandsServices.commandCtxManager

    override fun execute(command: UploadArtifactCommand): Any? {
        val artifact = artifactService.readArtifactFromBytes(command.type, command.artifact)
        localEappService.uploadArtifact(
            ArtifactUploadDto(
                command.type,
                artifact,
                AppSourceKey(commandCtxManager.getSourceAppName(), command.source)
            )
        )
        return true
    }
}

@CommandType("${EcosAppsConstants.COMMANDS_PREFIX_EAPPS}.send-user-artifact")
class UploadArtifactCommand(
    val type: String,
    val artifact: ByteArray,
    val source: SourceKey
)
