package ru.citeck.ecos.apps.app.api

import ru.citeck.ecos.apps.EcosAppsConstants
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.apps.app.service.LocalAppService
import ru.citeck.ecos.commands.CommandExecutor
import ru.citeck.ecos.commands.annotation.CommandType
import ru.citeck.ecos.commons.utils.ZipUtils

class GetArtifactTypesDirCommandExecutor(factory: EcosAppsServiceFactory) :
    CommandExecutor<GetArtifactTypesDirCommand> {

    private val localAppService: LocalAppService = factory.localAppService

    override fun execute(command: GetArtifactTypesDirCommand): Any? {
        return GetArtifactTypesDirCommandResponse(ZipUtils.writeZipAsBytes(localAppService.getArtifactTypesDir()))
    }
}

@CommandType("${EcosAppsConstants.COMMANDS_PREFIX_APP}.get-artifact-types-dir")
class GetArtifactTypesDirCommand

class GetArtifactTypesDirCommandResponse(
    val typesDir: ByteArray
)
