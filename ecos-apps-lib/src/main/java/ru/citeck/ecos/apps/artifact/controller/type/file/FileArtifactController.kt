package ru.citeck.ecos.apps.artifact.controller.type.file

import mu.KotlinLogging
import ru.citeck.ecos.apps.artifact.ArtifactMeta
import ru.citeck.ecos.apps.artifact.controller.ArtifactController
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.io.file.EcosFile
import java.util.*
import java.util.stream.Collectors

class FileArtifactController : ArtifactController<FileArtifact, FileArtifactController.Config> {

    companion object {
        val log = KotlinLogging.logger {}
    }

    override fun read(root: EcosFile, config: Config): List<FileArtifact> {
        return root.findFiles()
            .stream()
            .map { ecosFile ->
                Optional.ofNullable(
                    try {
                        FileArtifact(ecosFile.getName(), ecosFile.readAsBytes())
                    } catch (e: Exception) {
                        log.error(e) { "File artifact reading failed. File: ${ecosFile.getPath()}" }
                        null
                    }
                )
            }
            .filter { it.isPresent }
            .map { it.get() }
            .collect(Collectors.toList())
    }

    override fun write(root: EcosFile, artifact: FileArtifact, config: Config) {
        root.createFile(artifact.path) { output ->
            output.write(artifact.data)
        }
    }

    override fun getMeta(artifact: FileArtifact, config: Config): ArtifactMeta {
        return ArtifactMeta.create {
            withId(artifact.path)
        }
    }

    data class Config(
        val config: ObjectData = ObjectData.create()
    )
}
