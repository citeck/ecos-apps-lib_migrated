package ru.citeck.ecos.apps.artifact.controller.patch.override

class OverrideMeta(
    val order: Float = -100f,
    val scope: String = ""
)
