package ru.citeck.ecos.apps.artifact.controller.type.binary

import ru.citeck.ecos.commons.data.ObjectData
import java.util.*

data class BinArtifact(
    val path: String,
    val meta: ObjectData,
    val data: ByteArray
) {
    override fun toString(): String {
        return "BinArtifact(path='$path' meta=$meta)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (javaClass != other?.javaClass) {
            return false
        }
        other as BinArtifact
        return path == other.path &&
            meta == other.meta
    }

    override fun hashCode(): Int {
        return Objects.hash(path, meta)
    }
}
