package ru.citeck.ecos.apps.artifact.type

import ru.citeck.ecos.apps.artifact.controller.ArtifactControllerDef
import ru.citeck.ecos.commons.data.MLText
import ru.citeck.ecos.commons.data.Version
import ru.citeck.ecos.commons.io.file.EcosFile

class VirtualTypeContext(
    private val id: String,
    private val controllerDef: ArtifactControllerDef
) : TypeContext {

    override fun getId() = id

    override fun getMeta() = TypeMeta(
        MLText.EMPTY,
        Version("1.0"),
        "",
        false,
        controllerDef
    )

    override fun getControllerType() = getMeta().controller.type

    override fun <T : Any> getControllerConfig(type: Class<T>): T {
        return getControllerConfig().getAs(type)
            ?: error("Config conversion failed. Config: ${getControllerConfig()}")
    }

    override fun getControllerConfig() = getMeta().controller.config

    override fun getContent(): EcosFile {
        error("Type is virtual. Content is not defined")
    }
}
