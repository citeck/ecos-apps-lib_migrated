package ru.citeck.ecos.apps.eapps.service

import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceInfo

interface ArtifactUpdater {

    fun artifactsForceUpdate(appInstanceId: String, source: ArtifactSourceInfo)
}
