package ru.citeck.ecos.apps.app.domain.artifact.source

enum class ArtifactSourceType {
    USER,
    ECOS_APP,
    APPLICATION
}
