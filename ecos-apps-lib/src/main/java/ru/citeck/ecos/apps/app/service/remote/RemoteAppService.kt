package ru.citeck.ecos.apps.app.service.remote

import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.apps.app.api.*
import ru.citeck.ecos.apps.app.domain.artifact.source.SourceKey
import ru.citeck.ecos.apps.app.domain.buildinfo.dto.BuildInfo
import ru.citeck.ecos.apps.app.domain.status.AppStatus
import ru.citeck.ecos.commands.dto.CommandError
import ru.citeck.ecos.commands.dto.CommandResult
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.utils.ZipUtils
import java.time.Duration
import java.time.Instant
import java.util.concurrent.CompletableFuture
import java.util.concurrent.Future

class RemoteAppService(services: EcosAppsServiceFactory) {

    private val commands = services.commandsServices.commandsService

    fun getBuildInfo(since: Instant = Instant.EPOCH): Future<List<BuildInfo>> {

        val commandRes = commands.executeForGroup {
            targetApp = "all"
            body = GetAppBuildInfoCommand(since)
            ttl = Duration.ofSeconds(3)
        }
        if (commandRes is CompletableFuture<*>) {
            return commandRes.thenApply { results ->

                @Suppress("UNCHECKED_CAST")
                results as List<CommandResult>

                val resultMap = LinkedHashMap<String, BuildInfo>()
                results.forEach { res ->
                    if (res.result.isObject) {
                        val info = res.getResultAs(GetAppBuildInfoCommandResp::class.java)?.buildInfo
                        info?.forEach {
                            val currentInfo = resultMap[it.repo]
                            if (currentInfo == null || currentInfo.buildDate.isBefore(it.buildDate)) {
                                resultMap[it.repo] = it
                            }
                        }
                    }
                }
                resultMap.values.toList()
            }
        }

        return CompletableFuture.completedFuture(emptyList())
    }

    fun getBuildInfo(appId: String, since: Instant = Instant.EPOCH): Future<List<BuildInfo>> {

        val commandRes = commands.execute {
            targetApp = appId
            body = GetAppBuildInfoCommand(since)
            ttl = Duration.ofSeconds(3)
        }
        if (commandRes is CompletableFuture<*>) {
            return commandRes.thenApply {
                (it as? CommandResult)?.getResultAs(GetAppBuildInfoCommandResp::class.java)?.buildInfo ?: emptyList()
            }
        }
        return CompletableFuture.completedFuture(emptyList())
    }

    fun getAppsStatus(): List<RemoteAppStatus> {

        val result = mutableListOf<RemoteAppStatus>()

        commands.executeForGroupSync {
            targetApp = "all"
            body = GetAppStatusCommand()
            ttl = Duration.ofSeconds(3)
        }.forEach {
            if (it.result.isObject) {
                val status = it.getResultAs(AppStatus::class.java)
                if (status != null) {
                    result.add(
                        RemoteAppStatus(
                            it.appName,
                            it.appInstanceId,
                            status
                        )
                    )
                }
            }
        }

        return result
    }

    fun getArtifactTypesDir(appInstanceId: String): EcosFile {

        val result = commands.executeSync {
            withTargetAppInstanceId(appInstanceId)
            body = GetArtifactTypesDirCommand()
        }

        result.throwPrimaryErrorIfNotNull()

        val typesDir = result.getResultAs(GetArtifactTypesDirCommandResponse::class.java)?.typesDir
            ?: error("GetArtifactTypesCommand executed with unexpected result: $result")

        return ZipUtils.extractZip(typesDir)
    }

    fun getArtifactsDir(
        appInstanceId: String,
        source: SourceKey,
        typesDir: EcosFile,
        since: Instant
    ): EcosFile {

        val typesDirZip = ZipUtils.writeZipAsBytes(typesDir)

        val result = commands.executeSync {
            withTargetAppInstanceId(appInstanceId)
            body = GetArtifactsDirCommand(source, typesDirZip, since)
        }

        result.throwPrimaryErrorIfNotNull()

        val artifactsDir = result.getResultAs(GetArtifactsDirCommandResponse::class.java)?.artifactsDir
            ?: error("GetArtifactsDirCommand executed with unexpected result: $result")

        return ZipUtils.extractZip(artifactsDir)
    }

    fun deployArtifact(appInstanceId: String, type: String, artifact: ByteArray): List<CommandError> {

        val result = commands.executeSync {
            withTargetAppInstanceId(appInstanceId)
            body = DeployArtifactCommand(type, artifact)
        }

        return result.errors
    }
}
