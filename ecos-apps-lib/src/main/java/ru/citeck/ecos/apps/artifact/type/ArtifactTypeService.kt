package ru.citeck.ecos.apps.artifact.type

import mu.KotlinLogging
import ru.citeck.ecos.apps.EcosAppsConstants
import ru.citeck.ecos.apps.artifact.type.serialization.script.EcosScriptExecutor
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.utils.ZipUtils
import java.util.concurrent.ConcurrentHashMap

class ArtifactTypeService {

    companion object {
        val log = KotlinLogging.logger {}
    }

    private val types = ConcurrentHashMap<String, TypeContext>()
    private val groovyClassLoader = EcosScriptExecutor()

    fun getAllTypes(): List<TypeContext> {
        return ArrayList(types.values)
    }

    fun loadTypes(typesDir: EcosFile): List<TypeContext> {
        val typesCtx = readTypes(typesDir)
        typesCtx.forEach { register(it) }
        return typesCtx
    }

    fun loadType(typeId: String, typeDirZip: ByteArray): TypeContext {
        return loadType(typeId, ZipUtils.extractZip(typeDirZip))
    }

    fun loadType(typeId: String, typeDir: EcosFile): TypeContext {
        val context = readType(typeId, typeDir)
        register(context)
        return context
    }

    fun readTypes(typesDir: EcosFile): List<TypeContext> {

        val result = ArrayList<TypeContext>()

        typesDir.findFiles("**/${EcosAppsConstants.TYPE_META_FILE}").forEach {
            try {

                val parent = it.getParent() ?: error("Parent is not found")

                val rootPath = typesDir.getPath()
                val artifactTypePath = parent.getPath()
                val typeId = rootPath.relativize(artifactTypePath).toString().replace('\\', '/')

                result.add(readType(typeId, parent))
            } catch (e: Exception) {
                log.error(e) { "Artifact type reading failed: '${it.getPath()}'" }
            }
        }

        return result
    }

    fun readType(typeId: String, typeDir: EcosFile): TypeContext {

        val metaFile = typeDir.getFile(EcosAppsConstants.TYPE_META_FILE)
            ?: error("${EcosAppsConstants.TYPE_META_FILE} is not found in dir ${typeDir.getPath()}. Type: $typeId")

        return TypeContextImpl(typeId, metaFile, groovyClassLoader)
    }

    fun readType(typeId: String, typeDirZip: ByteArray): TypeContext {
        return readType(typeId, ZipUtils.extractZip(typeDirZip))
    }

    fun register(typeCtx: TypeContext) {
        types[typeCtx.getId()] = typeCtx
    }

    fun getType(id: String): TypeContext? {
        return types[id]
    }
}
