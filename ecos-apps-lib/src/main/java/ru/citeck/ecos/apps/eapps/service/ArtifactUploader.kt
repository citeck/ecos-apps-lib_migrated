package ru.citeck.ecos.apps.eapps.service

import ru.citeck.ecos.apps.eapps.dto.ArtifactUploadDto

interface ArtifactUploader {

    fun uploadArtifact(upload: ArtifactUploadDto)
}
