package ru.citeck.ecos.apps.app.domain.buildinfo.dto

import ecos.com.fasterxml.jackson210.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonDeserialize as JackDeserialize

@JsonDeserialize(builder = CommitInfo.Builder::class)
@JackDeserialize(builder = CommitInfo.Builder::class)
data class CommitInfo(
    val commit: String,
    val subject: String,
    val body: String,
    val author: CommitPersonInfo,
    val committer: CommitPersonInfo
) {

    companion object {

        @JvmStatic
        fun create(builder: Builder.() -> Unit): CommitInfo {
            return Builder().apply(builder).build()
        }
    }

    fun copy(): Builder {
        return Builder(this)
    }

    fun copy(builder: Builder.() -> Unit): CommitInfo {
        val builderObj = Builder(this)
        builder.invoke(builderObj)
        return builderObj.build()
    }

    class Builder() {

        var commit: String = ""
        var subject: String = ""
        var body: String = ""
        var author: CommitPersonInfo = CommitPersonInfo.EMPTY
        var committer: CommitPersonInfo = CommitPersonInfo.EMPTY

        constructor(base: CommitInfo) : this() {
            this.commit = base.commit
            this.subject = base.subject
            this.body = base.body
            this.author = base.author
            this.committer = base.committer
        }

        fun withCommit(commit: String?): Builder {
            this.commit = commit ?: ""
            return this
        }

        fun withSubject(subject: String?): Builder {
            this.subject = subject ?: ""
            return this
        }

        fun withBody(body: String?): Builder {
            this.body = body ?: ""
            return this
        }

        fun withAuthor(author: CommitPersonInfo?): Builder {
            this.author = author ?: CommitPersonInfo.EMPTY
            return this
        }

        fun withCommitter(committer: CommitPersonInfo?): Builder {
            this.committer = committer ?: CommitPersonInfo.EMPTY
            return this
        }

        fun build(): CommitInfo {
            return CommitInfo(commit, subject, body, author, committer)
        }
    }
}
