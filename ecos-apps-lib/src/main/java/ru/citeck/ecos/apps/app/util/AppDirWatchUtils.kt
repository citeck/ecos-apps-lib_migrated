package ru.citeck.ecos.apps.app.util

import mu.KotlinLogging
import java.nio.file.*
import java.util.*
import java.util.concurrent.CopyOnWriteArrayList
import java.util.function.BiConsumer
import kotlin.collections.LinkedHashMap
import kotlin.concurrent.thread

object AppDirWatchUtils {

    private val log = KotlinLogging.logger {}

    private val watchRecords = CopyOnWriteArrayList<WatchRecord>()

    @JvmStatic
    fun watchJ(
        id: String,
        directory: Path,
        eventKinds: List<WatchEvent.Kind<Path>>,
        action: BiConsumer<Path, WatchEvent<Path>>
    ) {

        watch(id, listOf(directory), eventKinds) { path, event ->
            action.accept(path, event)
        }
    }

    @JvmStatic
    fun watchJ(
        id: String,
        directories: List<Path>,
        eventKinds: List<WatchEvent.Kind<Path>>,
        action: BiConsumer<Path, WatchEvent<Path>>
    ) {

        watch(id, directories, eventKinds) { path, event ->
            action.accept(path, event)
        }
    }

    fun watch(
        id: String,
        directory: Path,
        eventKinds: List<WatchEvent.Kind<Path>>,
        action: (file: Path, event: WatchEvent<Path>) -> Unit
    ) {

        return watch(id, listOf(directory), eventKinds, action)
    }

    fun watch(
        id: String,
        directories: List<Path>,
        eventKinds: List<WatchEvent.Kind<Path>>,
        action: (file: Path, event: WatchEvent<Path>) -> Unit
    ) {

        addWatchRecord(id, directories, eventKinds, action)
    }

    private fun addWatchRecord(
        id: String,
        directories: List<Path>,
        eventKinds: List<WatchEvent.Kind<Path>>,
        action: (file: Path, event: WatchEvent<Path>) -> Unit
    ) {

        val watchService = FileSystems.getDefault().newWatchService()
        val pathByKey = HashMap<WatchKey, Path>()

        directories.forEach {
            it.toFile().walkTopDown()
                .filter { file -> file.isDirectory }
                .forEach { dir ->
                    pathByKey[
                        dir.toPath().register(
                            watchService,
                            *eventKinds.toTypedArray()
                        )
                    ] = dir.toPath()
                }
        }

        watchRecords.add(
            WatchRecord(
                id,
                watchService,
                pathByKey,
                action
            )
        )

        if (watchRecords.size == 1) {
            thread(
                start = true,
                name = "AppDirWatcher"
            ) {
                watchRecordsImpl()
            }
        }
    }

    private fun watchRecordsImpl() {

        while (true) {
            if (watchRecords.isEmpty()) {
                Thread.sleep(500)
                continue
            }
            watchRecords.forEach {
                watchRecordImpl(it)
            }
            Thread.sleep(100)
        }
    }

    private fun watchRecordImpl(record: WatchRecord) {

        var key: WatchKey? = record.watchService.poll() ?: return
        val eventsMap = LinkedHashMap<Pair<Path, WatchEvent.Kind<Path>>, WatchEvent<Path>>()

        while (key != null) {

            val dirPath = record.pathByKey[key]

            if (dirPath != null) {

                val events = key.pollEvents()

                for (event in events) {
                    @Suppress("UNCHECKED_CAST")
                    val pathEvent = event as WatchEvent<Path>
                    val fileName = pathEvent.context().fileName.toString()
                    if (fileName.endsWith("~")) {
                        continue
                    }
                    eventsMap[Pair(dirPath.resolve(fileName), pathEvent.kind())] = pathEvent
                }
            }
            key.reset()
            key = record.watchService.poll()
            if (key == null) {
                Thread.sleep(100)
                key = record.watchService.poll()
            }
        }
        try {
            eventsMap.forEach { (pathAndKind, event) ->
                record.action.invoke(pathAndKind.first, event)
            }
        } catch (e: Exception) {
            log.warn(e) { "Exception in AppDirWatch action" }
        }
    }

    private class WatchRecord(
        val id: String,
        val watchService: WatchService,
        val pathByKey: Map<WatchKey, Path>,
        val action: (file: Path, event: WatchEvent<Path>) -> Unit
    )
}
