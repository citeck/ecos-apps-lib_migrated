package ru.citeck.ecos.apps.eapps.api

import ru.citeck.ecos.apps.EcosAppsConstants
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceInfo
import ru.citeck.ecos.commands.CommandExecutor
import ru.citeck.ecos.commands.annotation.CommandType

class ArtifactsForceUpdateCommandExecutor(services: EcosAppsServiceFactory) :
    CommandExecutor<ArtifactsForceUpdateCommand> {

    private val localEappService = services.localEappsService
    private val commandCtxManager = services.commandsServices.commandCtxManager

    override fun execute(command: ArtifactsForceUpdateCommand): Any? {
        localEappService.artifactsForceUpdate(commandCtxManager.getSourceAppInstanceId(), command.source)
        return true
    }
}

@CommandType("${EcosAppsConstants.COMMANDS_PREFIX_EAPPS}.artifacts-force-update")
class ArtifactsForceUpdateCommand(
    val source: ArtifactSourceInfo
)
