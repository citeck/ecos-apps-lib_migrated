package ru.citeck.ecos.apps.artifact.controller

import mu.KotlinLogging
import ru.citeck.ecos.apps.EcosAppsConstants
import ru.citeck.ecos.apps.artifact.ArtifactMeta
import ru.citeck.ecos.apps.artifact.controller.patch.ArtifactPatch
import ru.citeck.ecos.apps.artifact.controller.patch.ArtifactPatchController
import ru.citeck.ecos.apps.artifact.controller.patch.override.OverridePatch
import ru.citeck.ecos.apps.artifact.type.TypeContext
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.json.Json
import ru.citeck.ecos.commons.utils.ReflectUtils
import ru.citeck.ecos.commons.utils.ZipUtils
import java.util.concurrent.ConcurrentHashMap

class ArtifactControllerService {

    companion object {
        val log = KotlinLogging.logger {}
    }

    private val controllers = ConcurrentHashMap<String, ControllerWithTypes>()

    fun read(artifact: ByteArray, typeCtx: TypeContext): List<Any> {
        val artifactDir = ZipUtils.extractZip(artifact)
        return read(artifactDir, typeCtx)
    }

    fun read(root: EcosFile, typeCtx: TypeContext): List<Any> {

        val controller = controllers[typeCtx.getControllerType()]

        if (controller == null) {
            log.error {
                "Controller with type '" + typeCtx.getControllerType() +
                    "' is not found! Read can't be performed"
            }
            return emptyList()
        }

        val config = typeCtx.getControllerConfig(controller.configType)

        return controller.controller.read(root, config)
    }

    fun write(root: EcosFile, typeCtx: TypeContext, artifact: Any) {

        val controller = controllers[typeCtx.getControllerType()]
        if (controller == null) {
            log.error {
                "Controller with type '" + typeCtx.getControllerType() +
                    "' is not found! Write can't be performed for $artifact"
            }
            return
        }

        val config = typeCtx.getControllerConfig(controller.configType)
        val convertedArtifact = Json.mapper.convert(artifact, controller.artifactType) ?: artifact

        return controller.controller.write(root, convertedArtifact, config)
    }

    fun getMeta(typeCtx: TypeContext, artifact: Any): ArtifactMeta? {

        val controller = controllers[typeCtx.getControllerType()]
        if (controller == null) {
            log.error {
                "Controller with type '" + typeCtx.getControllerType() +
                    "' is not found! getMeta can't be performed for $artifact"
            }
            return null
        }

        val config = typeCtx.getControllerConfig(controller.configType)
        val convertedArtifact = Json.mapper.convert(artifact, controller.artifactType) ?: artifact

        return controller.controller.getMeta(convertedArtifact, config)
    }

    fun applyPatches(typeCtx: TypeContext, artifact: Any, patches: List<ArtifactPatch>): Any {

        val controller = controllers[typeCtx.getControllerType()]
        if (controller == null) {
            log.error {
                "Controller with type '" + typeCtx.getControllerType() +
                    "' is not found! Patches can't be applied for $artifact"
            }
            return artifact
        }
        val patchesToApply = if (!controller.allowPatches) {
            log.debug { "Controller with type '${typeCtx.getControllerType()}' is not support patching." }
            patches.filter { it.type == EcosAppsConstants.PATCH_TYPE_OVERRIDE }
        } else {
            patches
        }
        if (patchesToApply.isEmpty()) {
            return artifact
        }

        val config = typeCtx.getControllerConfig(controller.configType)
        val convertedArtifact = Json.mapper.convert(artifact, controller.artifactType) ?: artifact

        var resultArtifact: Any = convertedArtifact

        for (patch in patchesToApply) {
            try {
                resultArtifact = if (patch.type == EcosAppsConstants.PATCH_TYPE_OVERRIDE) {
                    val overridePatch = patch.config.getAs(OverridePatch::class.java) ?: continue
                    read(overridePatch.data, typeCtx)[0]
                } else {
                    @Suppress("UNCHECKED_CAST")
                    val patchController = controller.controller as ArtifactPatchController<Any, Any>
                    patchController.applyPatch(resultArtifact, config, patch)
                }
            } catch (e: Exception) {
                log.error(e) { "Patch applying failed. Patch: $patch Artifact: $artifact" }
            }
        }

        return resultArtifact
    }

    fun register(type: String, controller: ArtifactController<*, *>) {
        val args = ReflectUtils.getGenericArgs(controller::class.java, ArtifactController::class.java)
        @Suppress("UNCHECKED_CAST")
        val controllerWithAny = controller as ArtifactController<Any, Any>
        val allowPatches = controller is ArtifactPatchController<*, *>
        controllers[type] = ControllerWithTypes(args[0], args[1], controllerWithAny, allowPatches)
    }

    data class ControllerWithTypes(
        val artifactType: Class<*>,
        val configType: Class<*>,
        val controller: ArtifactController<Any, Any>,
        val allowPatches: Boolean
    )
}
