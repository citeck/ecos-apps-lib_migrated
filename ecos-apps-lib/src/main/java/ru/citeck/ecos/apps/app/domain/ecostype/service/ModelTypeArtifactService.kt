package ru.citeck.ecos.apps.app.domain.ecostype.service

import ru.citeck.ecos.records2.RecordRef

class ModelTypeArtifactService {

    var modelTypeArtifactResolver: ModelTypeArtifactResolver? = null

    fun getTypeArtifacts(typeRefs: List<RecordRef>): List<RecordRef> {

        val artifactsSet = HashSet<RecordRef>()
        val artifactsList = ArrayList<RecordRef>()
        typeRefs.forEach { typeRef ->
            getTypeArtifacts(typeRef)
                .filter { artifactsSet.add(it) }
                .forEach { artifactsList.add(it) }
        }

        return artifactsList
    }

    fun getTypeArtifacts(typeRef: RecordRef): List<RecordRef> {
        return modelTypeArtifactResolver?.getTypeArtifacts(typeRef) ?: emptyList()
    }
}
