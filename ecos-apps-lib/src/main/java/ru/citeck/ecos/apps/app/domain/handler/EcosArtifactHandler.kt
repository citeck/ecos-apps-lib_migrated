package ru.citeck.ecos.apps.app.domain.handler

import java.util.function.Consumer

interface EcosArtifactHandler<T : Any> {

    fun deployArtifact(artifact: T)

    fun listenChanges(listener: Consumer<T>)

    fun deleteArtifact(artifactId: String)

    fun getArtifactType(): String
}
