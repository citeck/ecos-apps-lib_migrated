package ru.citeck.ecos.apps.artifact

import mu.KotlinLogging
import ru.citeck.ecos.apps.EcosAppsConstants
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.apps.artifact.controller.patch.ArtifactPatch
import ru.citeck.ecos.apps.artifact.controller.patch.ArtifactPatchArtifact
import ru.citeck.ecos.apps.artifact.controller.patch.override.OverrideMeta
import ru.citeck.ecos.apps.artifact.controller.patch.override.OverridePatch
import ru.citeck.ecos.apps.artifact.type.TypeContext
import ru.citeck.ecos.commons.data.MLText
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.io.file.mem.EcosMemDir
import ru.citeck.ecos.commons.json.Json
import ru.citeck.ecos.commons.utils.ZipUtils
import java.util.HashMap

class ArtifactService(factory: EcosAppsServiceFactory) {

    companion object {
        val log = KotlinLogging.logger {}
    }

    private val artifactTypeService = factory.artifactTypeService
    private val artifactControllerService = factory.artifactControllerService

    private var artifactLocations: Map<String, String> = HashMap()

    fun loadTypes(typesDir: EcosFile): List<TypeContext> {
        return artifactTypeService.loadTypes(typesDir)
    }

    fun readTypes(typesDir: EcosFile): List<TypeContext> {
        return artifactTypeService.readTypes(typesDir)
    }

    fun getType(typeId: String): TypeContext? {
        return artifactTypeService.getType(typeId)
    }

    fun getTypeNotNull(typeId: String): TypeContext {
        return getType(typeId) ?: error("Type is not registered: '$typeId'")
    }

    fun readArtifacts(root: EcosFile, types: List<TypeContext>): Map<String, List<Any>> {
        return readArtifacts(listOf(root), types)
    }

    fun readArtifacts(roots: List<EcosFile>, types: List<TypeContext>): Map<String, List<Any>> {
        return readArtifacts(roots, types, true)
    }

    private fun readArtifacts(
        roots: List<EcosFile>,
        types: List<TypeContext>,
        withOverrides: Boolean
    ): Map<String, List<Any>> {

        val result = mutableMapOf<String, List<Any>>()
        types.forEach { type ->
            try {
                result[type.getId()] = readArtifacts(roots, type)
            } catch (e: Exception) {
                val paths = try {
                    roots.map { it.getPath().toString() }
                } catch (e: Exception) {
                    log.error(e) { "Roots paths mapping failed" }
                    emptyList<String>()
                }
                log.error(e) { "Artifacts reading failed. Type: '${type.getId()}'. Roots: $paths" }
                result[type.getId()] = emptyList()
            }
        }
        if (withOverrides && types.any { it.getId() == EcosAppsConstants.ARTIFACT_PATCH_ARTIFACT_TYPE }) {
            val overrides = readOverrides(roots, types)
            if (overrides.isNotEmpty()) {
                val patches = result.computeIfAbsent(EcosAppsConstants.ARTIFACT_PATCH_ARTIFACT_TYPE) {
                    emptyList()
                }.toMutableList()
                patches.addAll(overrides)
                result[EcosAppsConstants.ARTIFACT_PATCH_ARTIFACT_TYPE] = patches
            }
        }

        return result
    }

    private fun readOverrides(roots: List<EcosFile>, types: List<TypeContext>): List<ArtifactPatchArtifact> {
        val result = ArrayList<ArtifactPatchArtifact>()
        roots.forEach { result.addAll(readOverrides(it, types)) }
        return result
    }

    private fun readOverrides(root: EcosFile, types: List<TypeContext>): List<ArtifactPatchArtifact> {

        val patches = ArrayList<ArtifactPatchArtifact>()

        val overrideDir = root.getDir(EcosAppsConstants.DIR_OVERRIDE) ?: return emptyList()
        val overrideMetaFile = overrideDir.getFile("meta.yml")
        val overrideMeta = if (overrideMetaFile != null) {
            Json.mapper.read(overrideMetaFile, OverrideMeta::class.java)
                ?: error("Incorrect meta file: ${overrideMetaFile.getPath()}")
        } else {
            OverrideMeta()
        }
        val typeById = types.associateBy { it.getId() }
        val overrideArtifacts = readArtifacts(listOf(overrideDir), types, false)

        for ((typeId, artifacts) in overrideArtifacts) {
            val type = typeById[typeId] ?: continue
            for (artifact in artifacts) {
                val meta = getArtifactMeta(type, artifact) ?: continue
                val artifactData = writeArtifactAsBytes(type, artifact)
                val overridePatch = OverridePatch(artifactData)
                val targetRef = ArtifactRef.create(typeId, meta.id)
                val patchId = if (overrideMeta.scope.isNotEmpty()) {
                    EcosAppsConstants.DIR_OVERRIDE + "_" + overrideMeta.scope + "$" + targetRef
                } else {
                    EcosAppsConstants.DIR_OVERRIDE + "$" + targetRef
                }
                patches.add(
                    ArtifactPatchArtifact(
                        patchId,
                        MLText.EMPTY,
                        overrideMeta.order,
                        targetRef,
                        EcosAppsConstants.PATCH_TYPE_OVERRIDE,
                        ObjectData.create(overridePatch)
                    )
                )
            }
        }

        return patches
    }

    fun readArtifacts(roots: List<EcosFile>, type: TypeContext?): List<Any> {
        return roots.flatMap { readArtifacts(it, type) }
    }

    fun readArtifacts(root: EcosFile, type: TypeContext?): List<Any> {

        type ?: return emptyList()

        val typeId = type.getId()
        val typeRoot = root.getDir(artifactLocations.getOrDefault(typeId, typeId)) ?: return emptyList()
        return artifactControllerService.read(typeRoot, type)
    }

    fun readArtifactFromBytes(typeId: String, artifact: ByteArray): Any {
        return readArtifactFromBytes(getTypeNotNull(typeId), artifact)
    }

    fun readArtifactFromBytes(type: TypeContext, artifact: ByteArray): Any {
        return artifactControllerService.read(artifact, type)[0]
    }

    fun writeArtifactAsBytes(typeId: String, artifact: Any): ByteArray {
        return writeArtifactAsBytes(getTypeNotNull(typeId), artifact)
    }

    fun writeArtifactAsBytes(type: TypeContext, artifact: Any): ByteArray {
        val root = EcosMemDir()
        artifactControllerService.write(root, type, artifact)
        return ZipUtils.writeZipAsBytes(root)
    }

    fun writeArtifacts(root: EcosFile, artifacts: Map<String, List<Any>>) {
        artifacts.forEach { (typeId, artifacts) ->
            artifacts.forEach {
                writeArtifact(root, getTypeNotNull(typeId), it)
            }
        }
    }

    fun writeArtifact(root: EcosFile, type: TypeContext, artifact: Any) {
        val typeRoot = root.getOrCreateDir(type.getId())
        artifactControllerService.write(typeRoot, type, artifact)
    }

    fun getArtifactMeta(type: TypeContext, artifact: Any): ArtifactMeta? {
        return artifactControllerService.getMeta(type, artifact)
    }

    fun getArtifactMeta(type: String, artifact: Any): ArtifactMeta? {
        return artifactTypeService.getType(type)?.let { getArtifactMeta(it, artifact) }
    }

    fun applyPatches(type: TypeContext, artifact: Any, patches: List<ArtifactPatch>): Any {
        return artifactControllerService.applyPatches(type, artifact, patches)
    }

    fun applyPatches(type: String, artifact: Any, patches: List<ArtifactPatch>): Any {

        val typeCtx = artifactTypeService.getType(type)
        if (typeCtx == null) {
            log.error("Artifact type is not registered: '$type'")
            return artifact
        }

        return artifactControllerService.applyPatches(typeCtx, artifact, patches)
    }

    fun setArtifactLocations(locations: Map<String, String>) {
        this.artifactLocations = HashMap(locations)
    }
}
