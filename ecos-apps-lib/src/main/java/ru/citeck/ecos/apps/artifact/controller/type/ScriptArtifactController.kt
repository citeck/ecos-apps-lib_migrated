package ru.citeck.ecos.apps.artifact.controller.type

import ru.citeck.ecos.apps.artifact.ArtifactMeta
import ru.citeck.ecos.apps.artifact.controller.ArtifactController
import ru.citeck.ecos.apps.artifact.type.serialization.script.ScriptObjectInstance
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.json.Json

class ScriptArtifactController : ArtifactController<ObjectData, ScriptArtifactController.Config> {

    override fun read(root: EcosFile, config: Config): List<ObjectData> {

        val args = config.script.getGenericTypes(ArtifactController::class.java)
        val convertedConfig = Json.mapper.convert(config.config, args[1])!!

        return config.script.instance.read(root, convertedConfig).map {
            Json.mapper.convert(it, ObjectData::class.java)!!
        }
    }

    override fun write(root: EcosFile, artifact: ObjectData, config: Config) {

        val args = config.script.getGenericTypes(ArtifactController::class.java)
        val convertedConfig = Json.mapper.convert(config.config, args[1])!!
        val convertedArtifact = Json.mapper.convert(artifact, args[0])!!

        config.script.instance.write(root, convertedArtifact, convertedConfig)
    }

    override fun getMeta(artifact: ObjectData, config: Config): ArtifactMeta {

        val args = config.script.getGenericTypes(ArtifactController::class.java)
        val convertedConfig = Json.mapper.convert(config.config, args[1])
            ?: error("Config can't be converted to type '${args[1]}'. Config: ${config.config}")
        val convertedArtifact = Json.mapper.convert(artifact, args[0])
            ?: error("Artifact can't be converted to type '${args[0]}'. Artifact: $artifact")

        return config.script.instance.getMeta(convertedArtifact, convertedConfig)
    }

    data class Config(
        val script: ScriptObjectInstance<ArtifactController<Any, Any>>,
        val config: ObjectData = ObjectData.create()
    )
}
