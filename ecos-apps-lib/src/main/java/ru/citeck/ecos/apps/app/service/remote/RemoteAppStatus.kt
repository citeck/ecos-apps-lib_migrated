package ru.citeck.ecos.apps.app.service.remote

import ru.citeck.ecos.apps.app.domain.status.AppStatus

data class RemoteAppStatus(

    val appName: String,
    val appInstanceId: String,

    val status: AppStatus
)
