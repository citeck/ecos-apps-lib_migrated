package ru.citeck.ecos.apps.artifact

import ecos.com.fasterxml.jackson210.databind.annotation.JsonDeserialize
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.data.MLText
import ru.citeck.ecos.commons.data.Version
import ru.citeck.ecos.commons.utils.MandatoryParam
import ru.citeck.ecos.records2.RecordRef
import com.fasterxml.jackson.databind.annotation.JsonDeserialize as JackDeserialize

@JsonDeserialize(builder = ArtifactMeta.Builder::class)
@JackDeserialize(builder = ArtifactMeta.Builder::class)
data class ArtifactMeta private constructor(
    val id: String,
    val name: MLText,
    val tags: List<String>,
    val dependencies: List<RecordRef>,
    val version: Version,
    val system: Boolean
) {

    companion object {

        @JvmStatic
        fun create(): Builder {
            return Builder()
        }

        @JvmStatic
        fun create(builder: Builder.() -> Unit): ArtifactMeta {
            val builderObj = Builder()
            builder.invoke(builderObj)
            return builderObj.build()
        }
    }

    fun copy(): Builder {
        return Builder(this)
    }

    fun copy(builder: Builder.() -> Unit): ArtifactMeta {
        val builderObj = Builder(this)
        builder.invoke(builderObj)
        return builderObj.build()
    }

    class Builder() {

        var id: String = ""
        var name = MLText()
        var tags: List<String> = emptyList()
        var dependencies: List<RecordRef> = emptyList()
        var version = Version("1.0")
        var system = false

        constructor(base: ArtifactMeta) : this() {
            this.id = base.id
            this.name = base.name
            this.tags = DataValue.create(base.tags).asStrList()
            this.version = base.version
            this.system = base.system
            this.dependencies = DataValue.create(base.dependencies).asList(RecordRef::class.java)
        }

        fun withId(id: String?): Builder {
            this.id = id ?: ""
            return this
        }

        fun withName(name: MLText?): Builder {
            this.name = name ?: MLText()
            return this
        }

        fun withVersion(version: Version?): Builder {
            this.version = version ?: Version("1.0")
            return this
        }

        fun withTags(tags: List<String>?): Builder {
            this.tags = tags ?: emptyList()
            return this
        }

        fun withDependencies(dependencies: List<RecordRef>): Builder {
            this.dependencies = dependencies
            return this
        }

        fun withSystem(system: Boolean): Builder {
            this.system = system
            return this
        }

        fun build(): ArtifactMeta {
            MandatoryParam.checkString("id", id)
            return ArtifactMeta(id, name, tags, dependencies, version, system)
        }
    }
}
