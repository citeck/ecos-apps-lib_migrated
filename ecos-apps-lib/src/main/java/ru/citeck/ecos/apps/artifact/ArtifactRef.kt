package ru.citeck.ecos.apps.artifact

import ecos.com.fasterxml.jackson210.annotation.JsonCreator
import ecos.com.fasterxml.jackson210.annotation.JsonValue
import org.apache.commons.lang3.StringUtils
import java.io.Serializable
import java.util.*

class ArtifactRef private constructor(val type: String, val id: String) : Serializable {

    companion object {

        private const val serialVersionUID = -3588381934829089436L

        @JvmField
        val EMPTY = ArtifactRef("", "")

        private const val TYPE_DELIMITER = "$"

        @JvmStatic
        @JsonCreator
        fun valueOf(str: String): ArtifactRef {
            if (StringUtils.isBlank(str) || TYPE_DELIMITER == str) {
                return EMPTY
            }
            val delimIdx = str.indexOf(TYPE_DELIMITER)
            val type = str.substring(0, delimIdx)
            val id: String
            id = if (delimIdx == str.length - 1) {
                StringUtils.EMPTY
            } else {
                str.substring(delimIdx + 1)
            }
            return create(type, id)
        }

        @JvmStatic
        fun create(type: String, id: String?): ArtifactRef {
            require(type.isNotEmpty()) {
                "Type is a mandatory parameter"
            }
            return ArtifactRef(type, StringUtils.defaultString(id))
        }
    }

    @JsonValue
    override fun toString(): String {
        return if (this === EMPTY) {
            ""
        } else {
            type + TYPE_DELIMITER + id
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null || javaClass != other.javaClass) {
            return false
        }
        val artifactRef = other as ArtifactRef
        return type == artifactRef.type && id == artifactRef.id
    }

    override fun hashCode(): Int {
        return Objects.hash(type, id)
    }
}
