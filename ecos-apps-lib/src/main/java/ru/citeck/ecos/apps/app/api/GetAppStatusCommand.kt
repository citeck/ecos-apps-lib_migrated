package ru.citeck.ecos.apps.app.api

import ru.citeck.ecos.apps.EcosAppsConstants
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.apps.app.domain.status.AppStatus
import ru.citeck.ecos.apps.app.service.LocalAppService
import ru.citeck.ecos.commands.CommandExecutor
import ru.citeck.ecos.commands.annotation.CommandType

class GetAppStatusCommandExecutor(factory: EcosAppsServiceFactory) :
    CommandExecutor<GetAppStatusCommand> {

    private val localAppService: LocalAppService = factory.localAppService

    override fun execute(command: GetAppStatusCommand): AppStatus {
        return localAppService.getAppStatus()
    }
}

@CommandType("${EcosAppsConstants.COMMANDS_PREFIX_APP}.get-app-status")
class GetAppStatusCommand
