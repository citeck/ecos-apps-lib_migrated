package ru.citeck.ecos.apps.app.domain.artifact.source

data class AppSourceKey(
    val appName: String,
    val source: SourceKey
)
