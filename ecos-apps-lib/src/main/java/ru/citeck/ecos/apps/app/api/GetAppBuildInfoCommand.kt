package ru.citeck.ecos.apps.app.api

import ru.citeck.ecos.apps.EcosAppsConstants
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.apps.app.domain.buildinfo.dto.BuildInfo
import ru.citeck.ecos.apps.app.service.LocalAppService
import ru.citeck.ecos.commands.CommandExecutor
import ru.citeck.ecos.commands.annotation.CommandType
import java.time.Instant

class GetAppBuildInfoCommandExecutor(factory: EcosAppsServiceFactory) :
    CommandExecutor<GetAppBuildInfoCommand> {

    private val localAppService: LocalAppService = factory.localAppService

    override fun execute(command: GetAppBuildInfoCommand): GetAppBuildInfoCommandResp {

        return GetAppBuildInfoCommandResp(localAppService.getBuildInfo(command.since ?: Instant.EPOCH))
    }
}

@CommandType("${EcosAppsConstants.COMMANDS_PREFIX_APP}.get-app-build-info")
class GetAppBuildInfoCommand(val since: Instant?)

class GetAppBuildInfoCommandResp(val buildInfo: List<BuildInfo>)
