package ru.citeck.ecos.apps.app.domain.buildinfo.service

import ru.citeck.ecos.apps.app.domain.buildinfo.dto.BuildInfo
import java.time.Instant
import java.util.concurrent.CopyOnWriteArrayList

class BuildInfoService {

    private val buildInfoProviders: MutableList<BuildInfoProvider> = CopyOnWriteArrayList()

    fun getBuildInfo(since: Instant = Instant.EPOCH): List<BuildInfo> {

        val buildInfoByRepo = LinkedHashMap<String, BuildInfo>()
        buildInfoProviders.forEach { provider ->
            provider.getBuildInfo().forEach {
                if (it.buildDate.isAfter(since)) {
                    val currentInfo = buildInfoByRepo[it.repo]
                    if (currentInfo == null || currentInfo.buildDate.isBefore(it.buildDate)) {
                        buildInfoByRepo[it.repo] = it
                    }
                }
            }
        }

        return buildInfoByRepo.values.toList()
    }

    fun addProvider(provider: BuildInfoProvider) {
        this.buildInfoProviders.add(provider)
    }

    fun addProviders(provides: Collection<BuildInfoProvider>) {
        this.buildInfoProviders.addAll(provides)
    }
}
