package ru.citeck.ecos.apps.app.domain.buildinfo.service

import ru.citeck.ecos.apps.app.domain.buildinfo.dto.BuildInfo

interface BuildInfoProvider {

    fun getBuildInfo(): List<BuildInfo>
}
