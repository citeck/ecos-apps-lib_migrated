package ru.citeck.ecos.apps.app.domain.artifact.reader

import ru.citeck.ecos.apps.artifact.type.TypeContext
import ru.citeck.ecos.commons.io.file.EcosFile

interface ArtifactsReader {

    fun readArtifacts(root: EcosFile, types: List<TypeContext>): Map<String, List<Any>>
}
