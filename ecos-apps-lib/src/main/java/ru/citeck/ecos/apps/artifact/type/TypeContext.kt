package ru.citeck.ecos.apps.artifact.type

import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.io.file.EcosFile

interface TypeContext {

    fun getId(): String

    fun getMeta(): TypeMeta

    fun getControllerType(): String

    fun <T : Any> getControllerConfig(type: Class<T>): T

    fun getControllerConfig(): ObjectData

    fun getContent(): EcosFile
}
