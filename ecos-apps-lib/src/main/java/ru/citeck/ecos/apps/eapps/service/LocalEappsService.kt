package ru.citeck.ecos.apps.eapps.service

import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceInfo
import ru.citeck.ecos.apps.eapps.dto.ArtifactUploadDto

class LocalEappsService {

    var artifactUploader: ArtifactUploader? = null
    var artifactUpdater: ArtifactUpdater? = null

    fun uploadArtifact(artifact: ArtifactUploadDto) {
        artifactUploader?.uploadArtifact(artifact)
    }

    fun artifactsForceUpdate(appInstanceId: String, source: ArtifactSourceInfo) {
        artifactUpdater?.artifactsForceUpdate(appInstanceId, source)
    }
}
