package ru.citeck.ecos.apps.app.api

import ru.citeck.ecos.apps.EcosAppsConstants
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.commands.CommandExecutor
import ru.citeck.ecos.commands.annotation.CommandType

class DeleteArtifactCommandExecutor(services: EcosAppsServiceFactory) : CommandExecutor<DeleteArtifactCommand> {

    private val localAppService = services.localAppService

    override fun execute(command: DeleteArtifactCommand): DeleteArtifactResp {
        localAppService.deleteArtifact(command.type, command.artifactId)
        return DeleteArtifactResp(true)
    }
}

@CommandType("${EcosAppsConstants.COMMANDS_PREFIX_APP}.delete-artifact")
class DeleteArtifactCommand(
    val type: String,
    val artifactId: String
)

class DeleteArtifactResp(
    val success: Boolean
)
