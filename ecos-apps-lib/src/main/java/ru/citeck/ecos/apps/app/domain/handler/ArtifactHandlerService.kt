package ru.citeck.ecos.apps.app.domain.handler

import mu.KotlinLogging
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.apps.artifact.ArtifactRef
import ru.citeck.ecos.commons.json.Json
import ru.citeck.ecos.commons.utils.ReflectUtils
import java.lang.IllegalStateException
import java.util.concurrent.ConcurrentHashMap
import java.util.function.Consumer

class ArtifactHandlerService(factory: EcosAppsServiceFactory) {

    companion object {
        val log = KotlinLogging.logger {}

        private const val NOT_REGISTERED_MSG = "Artifact handler for type '%s' is not registered"
    }

    private val handlers = ConcurrentHashMap<String, HandlerInfo>()

    private val remoteEappService = factory.remoteEappsService
    private val artifactService = factory.artifactService

    fun getSupportedTypes(): List<String> {
        return handlers.keys().toList()
    }

    fun deleteArtifact(type: String, artifactId: String) {
        val handlerInfo = getHandler(type, ArtifactRef.create(type, artifactId))
        handlerInfo.handler.deleteArtifact(artifactId)
    }

    fun deployArtifact(type: String, artifact: Any) {

        val handlerInfo = getHandler(type, artifact)

        val typeCtx = artifactService.getType(type)
            ?: error("Artifact type is not registered: '$type'. Deploying is not allowed")
        val meta = artifactService.getArtifactMeta(typeCtx, artifact)
            ?: error("Artifact meta can't be received. Type: '$type' Artifact: $artifact. Deploying is not allowed")

        val convertedArtifact = Json.mapper.convert(artifact, handlerInfo.artifactClass)!!

        handlerInfo.isInDeployProcess = true
        try {
            log.info { "Deploy artifact ${meta.name}(${meta.id}) with type '$type'" }
            handlerInfo.handler.deployArtifact(convertedArtifact)
        } finally {
            handlerInfo.isInDeployProcess = false
        }
    }

    fun register(handler: EcosArtifactHandler<*>) {

        val genericArg = ReflectUtils.getGenericArg(handler::class.java, EcosArtifactHandler::class.java)

        @Suppress("UNCHECKED_CAST")
        handler as EcosArtifactHandler<Any>
        @Suppress("UNCHECKED_CAST")
        genericArg as Class<Any>

        val info = HandlerInfo(handler, genericArg)
        this.handlers[handler.getArtifactType()] = info

        handler.listenChanges(
            Consumer {
                if (!info.isInDeployProcess) {
                    onArtifactChanged(handler, it)
                }
            }
        )
    }

    private fun onArtifactChanged(handler: EcosArtifactHandler<Any>, artifact: Any) {
        remoteEappService.fireArtifactChangedByUser(handler.getArtifactType(), artifact)
    }

    private fun getHandler(type: String, artifactRef: ArtifactRef): HandlerInfo {
        return getHandler(type) { "Artifact will be ignored: $artifactRef" }
    }

    private fun getHandler(type: String, artifact: Any): HandlerInfo {
        return getHandler(type) { "Artifact will be ignored: $artifact" }
    }

    private fun getHandler(type: String): HandlerInfo {
        return getHandler(type) { "" }
    }

    private fun getHandler(type: String, errMsg: () -> String): HandlerInfo {
        return handlers[type] ?: throw IllegalStateException(NOT_REGISTERED_MSG.format(type) + " " + errMsg.invoke())
    }

    data class HandlerInfo(
        val handler: EcosArtifactHandler<Any>,
        val artifactClass: Class<Any>,
        var isInDeployProcess: Boolean = false
    )
}
