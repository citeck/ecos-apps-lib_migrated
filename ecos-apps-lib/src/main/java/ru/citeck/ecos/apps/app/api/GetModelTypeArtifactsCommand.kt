package ru.citeck.ecos.apps.app.api

import ru.citeck.ecos.apps.EcosAppsConstants
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.commands.CommandExecutor
import ru.citeck.ecos.commands.annotation.CommandType
import ru.citeck.ecos.records2.RecordRef

class GetModelTypeArtifactsCommandExecutor(factory: EcosAppsServiceFactory) :
    CommandExecutor<GetModelTypeArtifactsCommand> {

    private val localAppService = factory.localAppService

    override fun execute(command: GetModelTypeArtifactsCommand): GetModelTypeArtifactsCommandResponse {
        return GetModelTypeArtifactsCommandResponse(localAppService.getEcosTypeArtifacts(command.typeRefs))
    }
}

@CommandType("${EcosAppsConstants.COMMANDS_PREFIX_APP}.get-model-type-artifacts")
data class GetModelTypeArtifactsCommand(
    val typeRefs: List<RecordRef>
)

data class GetModelTypeArtifactsCommandResponse(
    val artifacts: List<RecordRef>
)
