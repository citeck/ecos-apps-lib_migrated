package ru.citeck.ecos.apps.app.domain.artifact.source

data class SourceKey(
    val id: String,
    val type: ArtifactSourceType
)
