package ru.citeck.ecos.apps.app.api

import ru.citeck.ecos.apps.EcosAppsConstants
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.apps.app.domain.artifact.source.SourceKey
import ru.citeck.ecos.apps.app.service.LocalAppService
import ru.citeck.ecos.commands.CommandExecutor
import ru.citeck.ecos.commands.annotation.CommandType
import ru.citeck.ecos.commons.utils.ZipUtils
import java.time.Instant

class GetArtifactsDirCommandExecutor(factory: EcosAppsServiceFactory) : CommandExecutor<GetArtifactsDirCommand> {

    private val localAppService: LocalAppService = factory.localAppService

    override fun execute(command: GetArtifactsDirCommand): GetArtifactsDirCommandResponse {

        val typesDir = ZipUtils.extractZip(command.typesDir)
        val artifacts = localAppService.getArtifactsDir(command.source, typesDir, command.since)

        return GetArtifactsDirCommandResponse(ZipUtils.writeZipAsBytes(artifacts))
    }
}

@CommandType("${EcosAppsConstants.COMMANDS_PREFIX_APP}.get-artifacts-dir")
class GetArtifactsDirCommand(
    val source: SourceKey,
    val typesDir: ByteArray,
    val since: Instant
)

class GetArtifactsDirCommandResponse(
    val artifactsDir: ByteArray
)
