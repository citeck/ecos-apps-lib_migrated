package ru.citeck.ecos.apps.app.service

import mu.KotlinLogging
import ru.citeck.ecos.apps.EcosAppsConstants
import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.apps.app.domain.artifact.reader.ArtifactsReader
import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceInfo
import ru.citeck.ecos.apps.app.domain.artifact.source.SourceKey
import ru.citeck.ecos.apps.app.domain.artifact.type.ArtifactTypeProvider
import ru.citeck.ecos.apps.app.domain.buildinfo.dto.BuildInfo
import ru.citeck.ecos.apps.app.domain.status.AppStatus
import ru.citeck.ecos.apps.artifact.controller.ArtifactControllerDef
import ru.citeck.ecos.apps.artifact.controller.patch.ArtifactPatch
import ru.citeck.ecos.apps.artifact.type.TypeContext
import ru.citeck.ecos.apps.artifact.type.VirtualTypeContext
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.io.file.mem.EcosMemDir
import ru.citeck.ecos.commons.io.file.std.EcosStdFile
import ru.citeck.ecos.commons.json.Json
import ru.citeck.ecos.records2.RecordRef
import java.io.File
import java.time.Instant

class LocalAppService(services: EcosAppsServiceFactory) {

    companion object {
        val log = KotlinLogging.logger {}

        private val startTime = Instant.now()
    }

    var typeProvider: ArtifactTypeProvider? = null

    private val sourceProviders = services.artifactSourceProviders

    private val artifactService = services.artifactService
    private val artifactHandlerService = services.artifactHandlerService
    private val artifactTypeService = services.artifactTypeService
    private val buildInfoService = services.buildInfoService

    private val ecosTypeArtifactService = services.ecosTypeArtifactService

    private val serviceInitTime = Instant.now()

    init {
        val reader: ArtifactsReader = object : ArtifactsReader {
            override fun readArtifacts(root: EcosFile, types: List<TypeContext>): Map<String, List<Any>> {
                return services.artifactService.readArtifacts(root, types)
            }
        }
        sourceProviders.forEach { it.init(reader) }
    }

    // ecos type

    fun getEcosTypeArtifacts(typeRefs: List<RecordRef>): List<RecordRef> {
        return ecosTypeArtifactService.getTypeArtifacts(typeRefs)
    }

    // build info

    fun getBuildInfo(since: Instant = Instant.EPOCH): List<BuildInfo> {
        return buildInfoService.getBuildInfo(since)
    }

    // status

    fun getAppStatus(): AppStatus {

        return AppStatus(
            typeProvider?.getLastModified() ?: serviceInitTime,
            artifactHandlerService.getSupportedTypes(),
            getArtifactSources(),
            startTime
        )
    }

    // source

    fun getArtifactSources(): List<ArtifactSourceInfo> {
        val sources = ArrayList<ArtifactSourceInfo>()
        sourceProviders.forEach { sources.addAll(it.getArtifactSources()) }
        return sources
    }

    fun getArtifactsDir(source: SourceKey, types: List<TypeContext>, since: Instant): EcosFile {
        val typesDir = EcosMemDir()
        types.forEach {
            typesDir.createDir(it.getId()).copyFilesFrom(it.getContent())
        }
        return getArtifactsDir(source, typesDir, since)
    }

    fun getArtifactsDir(source: SourceKey, typesDir: EcosFile, since: Instant): EcosFile {
        val types = artifactService.loadTypes(typesDir)
        val result = EcosMemDir()
        sourceProviders.forEach { provider ->
            val artifactsMap = provider.getArtifacts(source, types, since)
            types.forEach {
                artifactsMap[it.getId()]?.forEach { artifact ->
                    try {
                        artifactService.writeArtifact(result, it, artifact)
                    } catch (e: Exception) {
                        log.error(e) { "Artifact write error. Type: $it Artifact: $artifact" }
                    }
                }
            }
        }
        return result
    }

    // types

    fun getArtifactTypesDir(): EcosFile {
        return typeProvider?.getArtifactTypesDir() ?: EcosMemDir()
    }

    // delete

    fun deleteArtifact(type: String, artifactId: String) {
        artifactHandlerService.deleteArtifact(type, artifactId)
    }

    // deploy

    fun deployArtifact(type: String, artifact: ByteArray) {
        val artifactToDeploy = artifactService.readArtifactFromBytes(type, artifact)
        artifactHandlerService.deployArtifact(type, artifactToDeploy)
    }

    // local

    fun readStaticLocalArtifacts(type: String, controllerType: String, controllerConfig: Any): List<Any> {

        val controllerDef = ArtifactControllerDef(controllerType, ObjectData.create(controllerConfig))

        val result = ArrayList<Any>()
        val patches = HashMap<String, MutableList<ArtifactPatch>>()

        val typeCtx = VirtualTypeContext(type, controllerDef)
        val patchCtx = VirtualTypeContext(
            EcosAppsConstants.ARTIFACT_PATCH_ARTIFACT_TYPE,
            ArtifactControllerDef("json")
        )

        if (sourceProviders.isEmpty()) {
            log.warn { "Source providers is empty. Nothing will be returned. Type: $type Controller: $controllerDef" }
            return emptyList()
        }

        val expectedPatchTargetPrefix = "$type$"

        for (provider in sourceProviders) {

            if (!provider.isStatic()) {
                continue
            }

            provider.getArtifactSources().forEach { source ->

                val artifacts = provider.getArtifacts(
                    source.key,
                    listOf(typeCtx, patchCtx),
                    Instant.EPOCH
                )
                result.addAll(artifacts[type] ?: emptyList())

                val patchArtifacts = artifacts[EcosAppsConstants.ARTIFACT_PATCH_ARTIFACT_TYPE] ?: emptyList()
                patchArtifacts.forEach {
                    if (it is ObjectData) {
                        val target = it.get("target").asText()
                        if (target.startsWith(expectedPatchTargetPrefix)) {
                            val targetId = target.replaceFirst(expectedPatchTargetPrefix, "")
                            val artifactPatch = Json.mapper.convert(it, ArtifactPatch::class.java)
                            if (artifactPatch != null) {
                                patches.computeIfAbsent(targetId) { ArrayList() }.add(artifactPatch)
                            }
                        }
                    }
                }
            }
        }

        if (patches.isNotEmpty()) {

            val resultsById = LinkedHashMap<String, Any>()
            result.forEach {
                val meta = artifactService.getArtifactMeta(typeCtx, it)
                if (meta != null) {
                    resultsById[meta.id] = it
                } else {
                    log.warn { "Artifact meta is null. Type: $type Artifact: $it" }
                }
            }

            patches.forEach { (targetId, artifactPatches) ->

                val patchesList = ArrayList(artifactPatches)

                val artifact = resultsById[targetId]
                if (artifact != null && patchesList.isNotEmpty()) {
                    patchesList.sortBy { it.order }
                    resultsById[targetId] = artifactService.applyPatches(typeCtx, artifact, patchesList)
                }
            }

            return resultsById.values.toList()
        } else {

            return result
        }
    }

    fun <T : Any> readLocalArtifacts(type: String, clazz: Class<T>): List<T> {

        val logInfo = { msg: String -> log.info { "Local artifacts: $msg" } }

        logInfo.invoke("Read artifacts of type '$type'")

        // load local types
        getAllTypes()

        val typeCtx = artifactTypeService.getType(type) ?: error("Type is not found: '$type'")

        val result = ArrayList<Any>()

        for (provider in sourceProviders) {

            getArtifactSources().forEach { source ->

                val artifacts = provider.getArtifacts(
                    source.key,
                    listOf(typeCtx),
                    Instant.EPOCH
                )[type] ?: emptyList()

                logInfo.invoke("Found ${artifacts.size} artifacts within source $source")

                result.addAll(artifacts)
            }
        }

        return Json.mapper.convert(result, Json.mapper.getListType(clazz)) ?: emptyList()
    }

    /**
     * Deploy artifacts without ecos-apps service.
     * Main purpose of this method - integration tests.
     */
    fun deployLocalArtifacts() {

        val logInfo = { msg: String -> log.info { "Local artifacts: $msg" } }

        logInfo.invoke("Deploy started")

        val types = getAllTypes()

        logInfo.invoke("Types found: ${types.map { it.getId() }}")

        val sources = getArtifactSources()

        logInfo.invoke("Sources count: ${sources.size}")
        sources.forEach { sourceInfo ->

            logInfo.invoke("Load artifacts from source: ${sourceInfo.key}")

            val artifactsDir = getArtifactsDir(sourceInfo.key, types, Instant.EPOCH)

            types.forEach { typeCtx ->

                logInfo.invoke("Load artifacts with type ${typeCtx.getId()}")

                val artifacts = artifactService.readArtifacts(artifactsDir, typeCtx)

                logInfo.invoke("Found ${artifacts.size} artifacts of type ${typeCtx.getId()}")

                artifacts.forEach {
                    try {
                        artifactHandlerService.deployArtifact(typeCtx.getId(), it)
                    } catch (e: Exception) {
                        log.error(e) { "Artifact deploy failed. Artifact: $it" }
                    }
                }
            }
        }
    }

    fun deployLocalArtifacts(artifactsRoot: File) {

        val logInfo = { msg: String -> log.info { "Local artifacts: $msg" } }

        logInfo.invoke("Deploy local artifacts from directory: ${artifactsRoot.absolutePath}")

        val types = getAllTypes()
        val artifactsRootDir = EcosStdFile(artifactsRoot)

        artifactService.readArtifacts(artifactsRootDir, types).forEach { (typeId, artifacts) ->

            logInfo.invoke("Deploy artifacts with type '$typeId'. Count: ${artifacts.size}")

            for (artifact in artifacts) {
                try {
                    artifactHandlerService.deployArtifact(typeId, artifact)
                } catch (e: Exception) {
                    log.error(e) { "Artifact deploy failed. Type: '$typeId' Artifact: $artifact" }
                }
            }
        }
    }

    private fun getAllTypes(): List<TypeContext> {
        var allTypes = artifactTypeService.getAllTypes()
        if (allTypes.isEmpty() && typeProvider != null) {
            allTypes = artifactTypeService.readTypes(getArtifactTypesDir())
        }
        return allTypes
    }
}
