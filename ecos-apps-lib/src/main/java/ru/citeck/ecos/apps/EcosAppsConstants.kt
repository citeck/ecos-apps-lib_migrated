package ru.citeck.ecos.apps

object EcosAppsConstants {

    const val EAPPS_SERVICE_NAME = "eapps"

    const val COMMANDS_PREFIX = "ecos-apps"
    const val COMMANDS_PREFIX_APP = "$COMMANDS_PREFIX.app"
    const val COMMANDS_PREFIX_EAPPS = "$COMMANDS_PREFIX.eapps"

    const val DIR_ARTIFACTS = "artifacts"
    const val DIR_TYPES = "types"
    const val DIR_EAPPS = "eapps"
    const val DIR_OVERRIDE = "override"

    const val ARTIFACT_PATCH_ARTIFACT_TYPE = "app/artifact-patch"

    const val PATCH_TYPE_OVERRIDE = "override"

    const val TYPE_META_FILE = "type.yml"
}
