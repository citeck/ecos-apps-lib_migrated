package ru.citeck.ecos.apps.artifact.controller.patch

interface ArtifactPatchController<T : Any, C : Any> {

    fun applyPatch(artifact: T, config: C, patch: ArtifactPatch): T
}
