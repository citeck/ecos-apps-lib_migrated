package ru.citeck.ecos.apps.app.domain.artifact.source

import ru.citeck.ecos.apps.app.domain.artifact.reader.ArtifactsReader
import ru.citeck.ecos.apps.artifact.type.TypeContext
import java.time.Instant

interface ArtifactSourceProvider {

    fun init(reader: ArtifactsReader)

    fun getArtifactSources(): List<ArtifactSourceInfo>

    fun getArtifacts(
        source: SourceKey,
        types: List<TypeContext>,
        since: Instant
    ): Map<String, List<Any>>

    fun listenChanges(listener: (ArtifactSourceInfo) -> Unit)

    fun isStatic(): Boolean
}
