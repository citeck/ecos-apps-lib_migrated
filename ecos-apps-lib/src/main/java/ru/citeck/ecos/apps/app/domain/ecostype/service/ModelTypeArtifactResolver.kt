package ru.citeck.ecos.apps.app.domain.ecostype.service

import ru.citeck.ecos.records2.RecordRef

interface ModelTypeArtifactResolver {

    fun getTypeArtifacts(typeRef: RecordRef): List<RecordRef>
}
