package ru.citeck.ecos.apps.artifact.controller.type.file

data class FileArtifact(
    val path: String,
    val data: ByteArray
) {
    override fun toString(): String {
        return "FileArtifact(path='$path')"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (javaClass != other?.javaClass) {
            return false
        }
        other as FileArtifact
        return path == other.path
    }

    override fun hashCode(): Int {
        return path.hashCode()
    }
}
