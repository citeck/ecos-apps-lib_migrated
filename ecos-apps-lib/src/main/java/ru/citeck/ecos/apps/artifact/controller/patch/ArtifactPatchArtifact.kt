package ru.citeck.ecos.apps.artifact.controller.patch

import ru.citeck.ecos.apps.artifact.ArtifactRef
import ru.citeck.ecos.commons.data.MLText
import ru.citeck.ecos.commons.data.ObjectData

data class ArtifactPatchArtifact(
    val id: String,
    val name: MLText,
    val order: Float = 0f,
    val target: ArtifactRef,
    val type: String,
    val config: ObjectData
)
