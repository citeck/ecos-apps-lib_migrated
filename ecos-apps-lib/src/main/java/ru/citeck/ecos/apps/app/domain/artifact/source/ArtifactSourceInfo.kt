package ru.citeck.ecos.apps.app.domain.artifact.source

import ecos.com.fasterxml.jackson210.databind.annotation.JsonDeserialize
import ru.citeck.ecos.commons.utils.MandatoryParam
import java.time.Instant
import com.fasterxml.jackson.databind.annotation.JsonDeserialize as JackDeserialize

@JsonDeserialize(builder = ArtifactSourceInfo.Builder::class)
@JackDeserialize(builder = ArtifactSourceInfo.Builder::class)
data class ArtifactSourceInfo(
    val key: SourceKey,
    val lastModified: Instant
) {

    companion object {

        @JvmStatic
        fun create(builder: Builder.() -> Unit): ArtifactSourceInfo {
            return Builder().apply(builder).build()
        }
    }

    fun copy(): Builder {
        return Builder(this)
    }

    fun copy(builder: Builder.() -> Unit): ArtifactSourceInfo {
        val builderObj = Builder(this)
        builder.invoke(builderObj)
        return builderObj.build()
    }

    class Builder() {

        private var key: SourceKey = SourceKey("", ArtifactSourceType.APPLICATION)
        private var lastModified: Instant = Instant.EPOCH

        constructor(base: ArtifactSourceInfo) : this() {
            key = base.key
            lastModified = base.lastModified
        }

        fun withKey(id: String, type: ArtifactSourceType): Builder {
            this.key = SourceKey(id, type)
            return this
        }

        fun withKey(key: SourceKey): Builder {
            this.key = key
            return this
        }

        fun withLastModified(modified: Instant): Builder {
            this.lastModified = modified
            return this
        }

        fun build(): ArtifactSourceInfo {
            MandatoryParam.checkString("id", key.id)
            return ArtifactSourceInfo(key, lastModified)
        }
    }
}
