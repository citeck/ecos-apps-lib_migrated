package ru.citeck.ecos.apps.artifact.type.serialization.script

import ecos.com.fasterxml.jackson210.core.JsonParser
import ecos.com.fasterxml.jackson210.databind.DeserializationContext
import ecos.com.fasterxml.jackson210.databind.deser.std.StdDeserializer
import ru.citeck.ecos.commons.io.file.EcosFile

class ScriptObjectInstanceDeserializer(
    private val scope: EcosFile,
    private val scriptExecutor: EcosScriptExecutor
) : StdDeserializer<ScriptObjectInstance<*>>(ScriptObjectInstance::class.java) {

    override fun deserialize(parser: JsonParser, ctx: DeserializationContext): ScriptObjectInstance<*> {
        val path = parser.valueAsString
        return ScriptObjectInstance<Any>(scriptExecutor.getObject(scope.getFile(path)!!))
    }
}
