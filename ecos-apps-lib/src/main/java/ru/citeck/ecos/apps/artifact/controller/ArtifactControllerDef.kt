package ru.citeck.ecos.apps.artifact.controller

import ru.citeck.ecos.commons.data.ObjectData

data class ArtifactControllerDef(
    val type: String,
    val config: ObjectData = ObjectData.create()
)
