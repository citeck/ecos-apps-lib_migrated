package ru.citeck.ecos.apps.test

import ru.citeck.ecos.apps.EcosAppsServiceFactory
import ru.citeck.ecos.apps.app.domain.artifact.reader.ArtifactsReader
import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceInfo
import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceProvider
import ru.citeck.ecos.apps.app.domain.artifact.source.ArtifactSourceType
import ru.citeck.ecos.apps.app.domain.artifact.source.SourceKey
import ru.citeck.ecos.apps.app.domain.artifact.type.ArtifactTypeProvider
import ru.citeck.ecos.apps.app.domain.handler.EcosArtifactHandler
import ru.citeck.ecos.apps.artifact.type.TypeContext
import ru.citeck.ecos.commands.CommandsProperties
import ru.citeck.ecos.commands.CommandsServiceFactory
import ru.citeck.ecos.commands.rabbit.RabbitCommandsService
import ru.citeck.ecos.commands.remote.RemoteCommandsService
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.io.file.mem.EcosMemDir
import ru.citeck.ecos.commons.io.file.std.EcosStdFile
import ru.citeck.ecos.commons.json.Json
import ru.citeck.ecos.rabbitmq.RabbitMqConn
import ru.citeck.ecos.records3.RecordsServiceFactory
import java.io.File
import java.time.Instant
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.function.Consumer

class EcosTestApp(private val root: File, val rabbitMqConn: RabbitMqConn) {

    private val services = object : EcosAppsServiceFactory() {
        override fun createArtifactSourceProviders(): List<ArtifactSourceProvider> {
            return createProviders()
        }
    }

    private var sourceLastModified: Instant = Instant.now()
    private var typesLastModified: Instant = Instant.now()

    private var deployedArtifacts = ConcurrentHashMap<String, MutableMap<String, Any>>()

    private val artifactListeners = ConcurrentHashMap<String, Consumer<Any>>()

    init {
        services.recordsServices = RecordsServiceFactory()
        services.commandsServices = object : CommandsServiceFactory() {

            override fun createProperties(): CommandsProperties {
                val props = super.createProperties()
                val instanceDelimIdx = root.name.indexOf("__")
                if (instanceDelimIdx > 0) {
                    props.appInstanceId = root.name
                    props.appName = root.name.substring(0, instanceDelimIdx)
                } else {
                    props.appInstanceId = root.name + "-" + UUID.randomUUID().toString()
                    props.appName = root.name
                }
                return props
            }

            override fun createRemoteCommandsService(): RemoteCommandsService {
                return RabbitCommandsService(this, rabbitMqConn)
            }
        }

        val typesDir = File(root, "types")
        if (typesDir.exists()) {
            val typeProvider = object : ArtifactTypeProvider {

                override fun getArtifactTypesDir(): EcosFile {
                    return EcosStdFile(File(root, "types"))
                }

                override fun getLastModified(): Instant? {
                    return typesLastModified
                }
            }
            services.localAppService.typeProvider = typeProvider
        }

        services.commandsServices.remoteCommandsService
        services.init()

        val types = services.artifactTypeService.readTypes(services.localAppService.getArtifactTypesDir())
        types.forEach {

            deployedArtifacts[it.getId()] = ConcurrentHashMap()

            services.artifactHandlerService.register(object : EcosArtifactHandler<Any> {
                override fun deployArtifact(artifact: Any) {
                    val meta = services.artifactControllerService.getMeta(it, artifact)!!
                    deployedArtifacts[it.getId()]!![meta.id] = artifact
                }
                override fun listenChanges(listener: Consumer<Any>) {
                    artifactListeners[it.getId()] = listener
                }
                override fun deleteArtifact(artifactId: String) {}
                override fun getArtifactType() = it.getId()
            })
        }

        services.artifactService.loadTypes(services.localAppService.getArtifactTypesDir())
    }

    fun addArtifactChangedByUser(type: String, artifact: Any) {

        val typeCtx = services.artifactService.getType(type)!!
        val meta = services.artifactControllerService.getMeta(typeCtx, artifact)!!

        deployedArtifacts[type]!![meta.id] = artifact
        artifactListeners[type]!!.accept(artifact)
    }

    fun <T : Any> getSourceArtifactsByType(typeId: String, clazz: Class<T>): List<T> {

        val type = services.artifactTypeService.getType(typeId) ?: return emptyList()

        val result = ArrayList<Any>()
        services.localAppService.getArtifactSources().forEach { source ->

            val types = EcosMemDir()
            types.createDir(type.getId()).copyFilesFrom(type.getContent())

            val artifactsDir = services.localAppService.getArtifactsDir(source.key, types, Instant.EPOCH)
            result.addAll(services.artifactService.readArtifacts(artifactsDir, type))
        }

        return result.map { Json.mapper.convert(it, clazz)!! }
    }

    fun getName(): String {
        return services.commandsServices.properties.appName
    }

    fun getInstanceId(): String {
        return services.commandsServices.properties.appInstanceId
    }

    fun <T : Any> getDeployedArtifacts(type: String, clazz: Class<T>): Map<String, T> {
        val mapType = Json.mapper.getMapType(String::class.java, clazz)
        return Json.mapper.convert(deployedArtifacts[type], mapType) ?: emptyMap()
    }

    fun setSourceLastModified(lastModified: Instant) {
        this.sourceLastModified = lastModified
    }

    fun setTypesLastModified(lastModified: Instant) {
        this.typesLastModified = lastModified
    }

    fun getServices(): EcosAppsServiceFactory {
        return services
    }

    fun dispose() {
        services.commandsServices.remoteCommandsService.dispose()
    }

    private fun createProviders(): List<ArtifactSourceProvider> {

        val artifactsDir = File(root, "artifacts")

        if (!artifactsDir.exists()) {
            return emptyList()
        }

        return listOf(object : ArtifactSourceProvider {

            lateinit var reader: ArtifactsReader

            override fun init(reader: ArtifactsReader) {
                this.reader = reader
            }

            override fun listenChanges(listener: (ArtifactSourceInfo) -> Unit) {
            }

            override fun isStatic(): Boolean = true

            override fun getArtifactSources(): List<ArtifactSourceInfo> {
                return listOf(
                    ArtifactSourceInfo.create {
                        withLastModified(sourceLastModified)
                        withKey(SourceKey("classpath", ArtifactSourceType.APPLICATION))
                    }
                )
            }
            override fun getArtifacts(source: SourceKey, types: List<TypeContext>, since: Instant): Map<String, List<Any>> {
                if ("classpath" != source.id) {
                    return emptyMap()
                }
                val allTypes = services.artifactTypeService.getAllTypes()
                return reader.readArtifacts(EcosStdFile(artifactsDir), allTypes)
            }
        })
    }
}
