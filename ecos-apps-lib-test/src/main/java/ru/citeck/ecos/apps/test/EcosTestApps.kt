package ru.citeck.ecos.apps.test

import ru.citeck.ecos.commons.io.file.mem.EcosMemDir
import ru.citeck.ecos.rabbitmq.RabbitMqConn
import java.io.File
import java.time.Instant

class EcosTestApps(root: File, rabbitMqConn: RabbitMqConn) {

    private val apps = (root.listFiles() ?: error("Incorrect root file: $root")).filter {
        it.isDirectory
    }.map {
        val app = EcosTestApp(it, rabbitMqConn)
        app.getName() to app
    }.toMap()

    fun getApp(id: String): EcosTestApp {
        return apps[id] ?: error("Application is not found: $id")
    }

    fun getApps(): Map<String, EcosTestApp> {
        return apps
    }

    fun getAllArtifactsFromProviders(): Map<String, List<Any>> {

        val allTypesDir = getAllTypesDir()
        val allArtifacts = HashMap<String, MutableList<Any>>()

        apps.values.forEach {
            it.getServices().localAppService.getArtifactSources().forEach { source ->

                val artifactsDir = it.getServices()
                    .localAppService
                    .getArtifactsDir(source.key, allTypesDir, Instant.EPOCH)

                val allTypes = it.getServices().artifactTypeService.loadTypes(allTypesDir)
                val artifacts = it.getServices().artifactService.readArtifacts(artifactsDir, allTypes)

                artifacts.forEach { entry ->
                    allArtifacts.computeIfAbsent(entry.key) { ArrayList() }.addAll(entry.value)
                }
            }
        }

        return allArtifacts
    }

    fun getAllTypesDir(): EcosMemDir {
        val allTypesDir = EcosMemDir()
        apps.values.forEach {
            allTypesDir.copyFilesFrom(it.getServices().localAppService.getArtifactTypesDir())
        }
        return allTypesDir
    }
}
