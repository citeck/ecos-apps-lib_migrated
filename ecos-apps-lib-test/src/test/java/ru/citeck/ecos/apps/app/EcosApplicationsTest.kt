package ru.citeck.ecos.apps.app

import com.github.fridujo.rabbitmq.mock.MockConnectionFactory
import org.junit.jupiter.api.Test
import ru.citeck.ecos.apps.test.EcosTestApps
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.rabbitmq.RabbitMqConn
import java.io.File
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

internal class EcosApplicationsTest {

    @Test
    fun test() {

        val rabbitConnectionFactory = MockConnectionFactory()
        rabbitConnectionFactory.host = "localhost"
        rabbitConnectionFactory.username = "admin"
        rabbitConnectionFactory.password = "admin"

        val rabbitMqConn = RabbitMqConn(rabbitConnectionFactory)
        rabbitMqConn.waitUntilReady(2_000)

        val allApps = EcosTestApps(File("./src/test/resources/apps"), rabbitMqConn)
        assertEquals(3, allApps.getApps().size)

        val appsStatus = allApps.getApp("app0").getServices().remoteAppService.getAppsStatus()
        assertEquals(3, appsStatus.size)

        val artifacts = allApps.getAllArtifactsFromProviders()
        assertEquals(3, artifacts.size)

        val forms = allApps.getApp("app0").getSourceArtifactsByType("ui/form", ObjectData::class.java)
        assertEquals(2, forms.size)
        assertNotNull(forms.find { it.get("id").asText() == "first-form" })
        assertNotNull(forms.find { it.get("id").asText() == "second-form" })
    }
}
